-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for osx10.14 (x86_64)
--
-- Host: localhost    Database: freeconomy
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `about_uses`
--

DROP TABLE IF EXISTS `about_uses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `about_uses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about_uses`
--

LOCK TABLES `about_uses` WRITE;
/*!40000 ALTER TABLE `about_uses` DISABLE KEYS */;
/*!40000 ALTER TABLE `about_uses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Icon` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES (1,'leaves','2020-05-20 09:35:29','2020-05-20 11:41:26'),(2,'hourglass','2020-05-20 12:12:13','2020-05-20 12:12:13'),(3,'homeHart','2020-05-20 12:15:26','2020-05-20 12:15:26'),(4,'shoppingBags','2020-05-20 12:19:14','2020-05-20 12:19:14'),(5,'recycle','2020-05-20 12:20:59','2020-05-20 12:20:59');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activities_components`
--

DROP TABLE IF EXISTS `activities_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities_components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field` varchar(255) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `component_type` varchar(255) NOT NULL,
  `component_id` int(11) NOT NULL,
  `activity_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_id_fk` (`activity_id`),
  CONSTRAINT `activity_id_fk` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities_components`
--

LOCK TABLES `activities_components` WRITE;
/*!40000 ALTER TABLE `activities_components` DISABLE KEYS */;
INSERT INTO `activities_components` VALUES (3,'i18n',1,'components_i18n_activities_i18ns',3,2),(4,'i18n',2,'components_i18n_activities_i18ns',4,2),(5,'i18n',1,'components_i18n_activities_i18ns',5,3),(6,'i18n',2,'components_i18n_activities_i18ns',6,3),(7,'i18n',1,'components_i18n_activities_i18ns',7,4),(8,'i18n',2,'components_i18n_activities_i18ns',8,4),(9,'i18n',1,'components_i18n_activities_i18ns',9,5),(10,'i18n',2,'components_i18n_activities_i18ns',10,5),(11,'i18n',1,'components_i18n_activities_i18ns',11,1),(12,'i18n',2,'components_i18n_activities_i18ns',12,1);
/*!40000 ALTER TABLE `activities_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `components_i18n_activities_i18ns`
--

DROP TABLE IF EXISTS `components_i18n_activities_i18ns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `components_i18n_activities_i18ns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `components_i18n_activities_i18ns`
--

LOCK TABLES `components_i18n_activities_i18ns` WRITE;
/*!40000 ALTER TABLE `components_i18n_activities_i18ns` DISABLE KEYS */;
INSERT INTO `components_i18n_activities_i18ns` VALUES (3,'TIME BANK','Our ** TIME BANK ** gathers the human and material resources of the members of the community, to be exchanged as currency. Our time is our resource of value. And everyone\'s time is equally valuable, so 1 hr of DIY equals 1 hr of any other service, be it massage, design, or language classes.\nOur Time Bank offers various services to the entire FREECONOMY MALLORCA community that are available on the website.','en'),(4,'BANCO DE TIEMPO','Nuestro **BANCO DE TIEMPO** reúne los recursos humanos y materiales de los miembros de la comunidad, para ser intercambiados como moneda. Nuestro tiempo es nuestro recurso de valor. Y el tiempo de todos y uno es igualmente valioso, por lo que 1 hr de bricolaje equivale a 1 hr de cualquier otro servicio, ya sea masaje, diseño, o clases de idiomas.\nNuestro Banco de tiempo ofrece servicios diversos a toda la comunidad de FREECONOMY MALLORCA que están disponibles en la página web.','es'),(5,'ECOALDEAS','El proyecto ECOALDEAS igualmente, busca la creación de comunidades de personas que construyan sus hogares de acuerdo con métodos de **BIOCONSTRUCCIÓN**, que hacen uso de materiales locales y ecológicos.','es'),(6,'ECO VILLAGE','The Eco Village project also seeks to create communities of people who build their homes according to ** BIOCONSTRUCTION ** methods, which make use of local and ecological materials.','en'),(7,'FREESHOP','Es un proyecto de FREECONOMY que ha funcionado maravillosamente en todos los grupos internacionales, como Irlanda y Holanda, adonde las personas llevan aquellos objetos que todavía están en buen estado, pero que ya no van a usar, y a cambio pueden llevarse de la tienda aquello que esté allí disponible.','es'),(8,'FREESHOP','It is a FREECONOMY project that has worked wonderfully in all international groups, such as Ireland and the Netherlands, where people take objects that are still in good condition, but are no longer going to be used, and in exchange they can take what they want from the store.','en'),(9,'GRUPO DE INTERCAMBIO DE OBJETOS Y MATERIALES','El Grupo de INTERCAMBIO de OBJETOS Y MATERIALES, (ToolShare) es una propuesta de intercambio entre los miembros y amigos de la comunidad que tienen la necesidad de usar herramientas como por ejemplo una pala para el jardín, o una podadora y, en lugar de comprar otra, la comparte con otro de los miembros del grupo que la necesite, con la condición de devolverla a su dueño, en buen estado.\nEste servicio está disponible en la Web para que los miembros puedan ponerse en contacto.','es'),(10,'TOOLSHARE','The OBJECTS AND MATERIALS EXCHANGE GROUP, (ToolShare) is an exchange proposal between the members and friends of the community who have the need to use tools such as a garden shovel, or a mower and, instead of buying another, shares it with another member of the group who needs it, on the condition that it be returned to its owner, in good condition.\nThis service is available on the Web for members to contact.','en'),(11,'HUERTAS ECOLOGICA','Nuestra comunidad está creciendo día a día, gracias a la colaboración voluntaria y desinteresada de los miembros, que desean realizar un cambio real en sus vidas que verdaderamente beneficie a todos, incluido el planeta y sus recursos naturales. \nPor este motivo, los proyectos comunitarios de ','es'),(12,'ORGANIC GARDENS','Our community is growing day by day, thanks to the voluntary and disinterested collaboration of the members, who want to make a real change in their lives that truly benefits everyone, including the planet and its natural resources. For this reason, the HUERTA ECOLOGICA community projects bring together human and natural resources using permaculture and Bioculture methods that optimize resources creating the least possible impact.','en');
/*!40000 ALTER TABLE `components_i18n_activities_i18ns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `components_i18n_missions`
--

DROP TABLE IF EXISTS `components_i18n_missions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `components_i18n_missions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL,
  `language` varchar(255) NOT NULL,
  `subtitle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `components_i18n_missions`
--

LOCK TABLES `components_i18n_missions` WRITE;
/*!40000 ALTER TABLE `components_i18n_missions` DISABLE KEYS */;
INSERT INTO `components_i18n_missions` VALUES (1,'QUE HACEMOS','Nuestro deseo es crear una comunidad local autosostenible, de personas dispuestas a actuar en beneficio de toda la comunidad sin interés monetario, y respetando los procesos ecológicos. Queremos crear vínculos con las personas, dentro y fuera de Mallorca y España, conocerlas y poco a poco crear relaciones humanas de colaboración que sean enriquecedoras y significativas.\n\nLas personas que ofrecemos nuestro tiempo y habilidades a este proyecto, disfrutamos de lo que hacemos y creemos que es posible colaborar con otros de manera desinteresada, por el gusto de hacerlo. \n\nNos importa el planeta, nos importan las personas y defendemos el desarrollo de la producción local, antes que la globalización de recursos, porque entendemos que esta forma de economía globalizada, con su sistema de deuda monetaria está destruyendo los ecosistemas poco a poco y también las relaciones humanas. Sabemos que existen alternativas más humanas y solidarias y que ya están funcionando de forma efectiva y satisfactoria, en comunidades en más de 150 países. Sabemos que es posible trasladar ese modelo de vida a nuestra comunidad local y crear una diferencia que salve al planeta de la destrucción y ofrezca un futuro real y próspero a nuestros hijos. También estamos plenamente convencidos en la capacidad del ser humano para crear y regenerarse como especie. Tenemos fe plenamente en la humanidad, como uno de los muchos recursos valiosos de este maravilloso planeta.','es','PROYECTOS FREECONOMY MALLORCA'),(2,'WHAT WE DO','Our desire is to create a self-sustaining local community of people willing to act for the benefit of the entire community without monetary interest, and respecting ecological processes. We want to create links with people, inside and outside Mallorca and Spain, get to know them and gradually create collaborative human relationships that are enriching and meaningful. The people who offer our time and skills to this project, enjoy what we do and believe that it is possible to collaborate with others in a selfless way, for the pleasure of doing it. \n\nWe care about the planet, we care about people and we defend the development of local production, before the globalization of resources, because we understand that this form of globalized economy, with its monetary debt system, is destroying ecosystems little by little and also the human relations. We know that there are more humane and supportive alternatives and that they are already working effectively and satisfactorily, in communities in more than 150 countries. We know that it is possible to transfer that model of life to our local community and create a difference that saves the planet from destruction and offers a real and prosperous future for our children. We are also fully convinced of the human being\'s ability to create and regenerate as a species. We have full faith in humanity as one of the many valuable resources of this wonderful planet.','en','FREECONOMY MALLORCA PROJECTS');
/*!40000 ALTER TABLE `components_i18n_missions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `components_page_content_heroes`
--

DROP TABLE IF EXISTS `components_page_content_heroes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `components_page_content_heroes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Slogan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `components_page_content_heroes`
--

LOCK TABLES `components_page_content_heroes` WRITE;
/*!40000 ALTER TABLE `components_page_content_heroes` DISABLE KEYS */;
/*!40000 ALTER TABLE `components_page_content_heroes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `components_page_content_media_objects`
--

DROP TABLE IF EXISTS `components_page_content_media_objects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `components_page_content_media_objects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) NOT NULL,
  `Body` longtext DEFAULT NULL,
  `IconType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `components_page_content_media_objects`
--

LOCK TABLES `components_page_content_media_objects` WRITE;
/*!40000 ALTER TABLE `components_page_content_media_objects` DISABLE KEYS */;
/*!40000 ALTER TABLE `components_page_content_media_objects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_store`
--

DROP TABLE IF EXISTS `core_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_store` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` longtext DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `environment` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_store`
--

LOCK TABLES `core_store` WRITE;
/*!40000 ALTER TABLE `core_store` DISABLE KEYS */;
INSERT INTO `core_store` VALUES (1,'db_model_strapi_webhooks','{\"name\":{\"type\":\"string\"},\"url\":{\"type\":\"text\"},\"headers\":{\"type\":\"json\"},\"events\":{\"type\":\"json\"},\"enabled\":{\"type\":\"boolean\"}}','object',NULL,NULL),(2,'db_model_users-permissions_permission','{\"type\":{\"type\":\"string\",\"required\":true,\"configurable\":false},\"controller\":{\"type\":\"string\",\"required\":true,\"configurable\":false},\"action\":{\"type\":\"string\",\"required\":true,\"configurable\":false},\"enabled\":{\"type\":\"boolean\",\"required\":true,\"configurable\":false},\"policy\":{\"type\":\"string\",\"configurable\":false},\"role\":{\"model\":\"role\",\"via\":\"permissions\",\"plugin\":\"users-permissions\",\"configurable\":false}}','object',NULL,NULL),(3,'db_model_core_store','{\"key\":{\"type\":\"string\"},\"value\":{\"type\":\"text\"},\"type\":{\"type\":\"string\"},\"environment\":{\"type\":\"string\"},\"tag\":{\"type\":\"string\"}}','object',NULL,NULL),(4,'db_model_upload_file','{\"name\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"alternativeText\":{\"type\":\"string\",\"configurable\":false},\"caption\":{\"type\":\"string\",\"configurable\":false},\"width\":{\"type\":\"integer\",\"configurable\":false},\"height\":{\"type\":\"integer\",\"configurable\":false},\"formats\":{\"type\":\"json\",\"configurable\":false},\"hash\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"ext\":{\"type\":\"string\",\"configurable\":false},\"mime\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"size\":{\"type\":\"decimal\",\"configurable\":false,\"required\":true},\"url\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"previewUrl\":{\"type\":\"string\",\"configurable\":false},\"provider\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"provider_metadata\":{\"type\":\"json\",\"configurable\":false},\"related\":{\"collection\":\"*\",\"filter\":\"field\",\"configurable\":false},\"created_at\":{\"type\":\"currentTimestamp\"},\"updated_at\":{\"type\":\"currentTimestamp\"}}','object',NULL,NULL),(5,'db_model_strapi_administrator','{\"username\":{\"type\":\"string\",\"minLength\":3,\"unique\":true,\"configurable\":false,\"required\":true},\"email\":{\"type\":\"email\",\"minLength\":6,\"configurable\":false,\"required\":true},\"password\":{\"type\":\"password\",\"minLength\":6,\"configurable\":false,\"private\":true,\"required\":true},\"resetPasswordToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true},\"blocked\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false}}','object',NULL,NULL),(6,'db_model_users-permissions_user','{\"username\":{\"type\":\"string\",\"minLength\":3,\"unique\":true,\"configurable\":false,\"required\":true},\"email\":{\"type\":\"email\",\"minLength\":6,\"configurable\":false,\"required\":true},\"provider\":{\"type\":\"string\",\"configurable\":false},\"password\":{\"type\":\"password\",\"minLength\":6,\"configurable\":false,\"private\":true},\"resetPasswordToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true},\"confirmed\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false},\"blocked\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false},\"role\":{\"model\":\"role\",\"via\":\"users\",\"plugin\":\"users-permissions\",\"configurable\":false},\"created_at\":{\"type\":\"currentTimestamp\"},\"updated_at\":{\"type\":\"currentTimestamp\"}}','object',NULL,NULL),(7,'db_model_users-permissions_role','{\"name\":{\"type\":\"string\",\"minLength\":3,\"required\":true,\"configurable\":false},\"description\":{\"type\":\"string\",\"configurable\":false},\"type\":{\"type\":\"string\",\"unique\":true,\"configurable\":false},\"permissions\":{\"collection\":\"permission\",\"via\":\"role\",\"plugin\":\"users-permissions\",\"configurable\":false,\"isVirtual\":true},\"users\":{\"collection\":\"user\",\"via\":\"role\",\"configurable\":false,\"plugin\":\"users-permissions\",\"isVirtual\":true}}','object',NULL,NULL),(8,'db_model_upload_file_morph','{\"upload_file_id\":{\"type\":\"integer\"},\"related_id\":{\"type\":\"integer\"},\"related_type\":{\"type\":\"text\"},\"field\":{\"type\":\"text\"},\"order\":{\"type\":\"integer\"}}','object',NULL,NULL),(9,'plugin_email_provider','{\"provider\":\"sendmail\",\"name\":\"Sendmail\",\"auth\":{\"sendmail_default_from\":{\"label\":\"Sendmail Default From\",\"type\":\"text\"},\"sendmail_default_replyto\":{\"label\":\"Sendmail Default Reply-To\",\"type\":\"text\"}}}','object','development',''),(10,'plugin_users-permissions_grant','{\"email\":{\"enabled\":true,\"icon\":\"envelope\"},\"discord\":{\"enabled\":false,\"icon\":\"discord\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/discord/callback\",\"scope\":[\"identify\",\"email\"]},\"facebook\":{\"enabled\":false,\"icon\":\"facebook-square\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/facebook/callback\",\"scope\":[\"email\"]},\"google\":{\"enabled\":false,\"icon\":\"google\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/google/callback\",\"scope\":[\"email\"]},\"github\":{\"enabled\":false,\"icon\":\"github\",\"key\":\"\",\"secret\":\"\",\"redirect_uri\":\"/auth/github/callback\",\"scope\":[\"user\",\"user:email\"]},\"microsoft\":{\"enabled\":false,\"icon\":\"windows\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/microsoft/callback\",\"scope\":[\"user.read\"]},\"twitter\":{\"enabled\":false,\"icon\":\"twitter\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/twitter/callback\"},\"instagram\":{\"enabled\":false,\"icon\":\"instagram\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/instagram/callback\"},\"vk\":{\"enabled\":false,\"icon\":\"vk\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/vk/callback\",\"scope\":[\"email\"]}}','object','',''),(11,'plugin_upload_settings','{\"sizeOptimization\":true,\"responsiveDimensions\":true}','object','development',''),(12,'plugin_content_manager_configuration_content_types::plugins::users-permissions.permission','{\"uid\":\"plugins::users-permissions.permission\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"type\",\"defaultSortBy\":\"type\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"type\":{\"edit\":{\"label\":\"Type\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Type\",\"searchable\":true,\"sortable\":true}},\"controller\":{\"edit\":{\"label\":\"Controller\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Controller\",\"searchable\":true,\"sortable\":true}},\"action\":{\"edit\":{\"label\":\"Action\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Action\",\"searchable\":true,\"sortable\":true}},\"enabled\":{\"edit\":{\"label\":\"Enabled\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Enabled\",\"searchable\":true,\"sortable\":true}},\"policy\":{\"edit\":{\"label\":\"Policy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Policy\",\"searchable\":true,\"sortable\":true}},\"role\":{\"edit\":{\"label\":\"Role\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Role\",\"searchable\":false,\"sortable\":false}}},\"layouts\":{\"list\":[\"id\",\"type\",\"controller\",\"action\"],\"editRelations\":[\"role\"],\"edit\":[[{\"name\":\"type\",\"size\":6},{\"name\":\"controller\",\"size\":6}],[{\"name\":\"action\",\"size\":6},{\"name\":\"enabled\",\"size\":4}],[{\"name\":\"policy\",\"size\":6}]]}}','object','',''),(13,'plugin_content_manager_configuration_content_types::plugins::users-permissions.role','{\"uid\":\"plugins::users-permissions.role\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"Description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Description\",\"searchable\":true,\"sortable\":true}},\"type\":{\"edit\":{\"label\":\"Type\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Type\",\"searchable\":true,\"sortable\":true}},\"permissions\":{\"edit\":{\"label\":\"Permissions\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"type\"},\"list\":{\"label\":\"Permissions\",\"searchable\":false,\"sortable\":false}},\"users\":{\"edit\":{\"label\":\"Users\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"username\"},\"list\":{\"label\":\"Users\",\"searchable\":false,\"sortable\":false}}},\"layouts\":{\"list\":[\"id\",\"name\",\"description\",\"type\"],\"editRelations\":[\"permissions\",\"users\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"description\",\"size\":6}],[{\"name\":\"type\",\"size\":6}]]}}','object','',''),(14,'plugin_content_manager_configuration_content_types::plugins::upload.file','{\"uid\":\"plugins::upload.file\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"alternativeText\":{\"edit\":{\"label\":\"AlternativeText\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"AlternativeText\",\"searchable\":true,\"sortable\":true}},\"caption\":{\"edit\":{\"label\":\"Caption\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Caption\",\"searchable\":true,\"sortable\":true}},\"width\":{\"edit\":{\"label\":\"Width\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Width\",\"searchable\":true,\"sortable\":true}},\"height\":{\"edit\":{\"label\":\"Height\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Height\",\"searchable\":true,\"sortable\":true}},\"formats\":{\"edit\":{\"label\":\"Formats\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Formats\",\"searchable\":false,\"sortable\":false}},\"hash\":{\"edit\":{\"label\":\"Hash\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Hash\",\"searchable\":true,\"sortable\":true}},\"ext\":{\"edit\":{\"label\":\"Ext\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Ext\",\"searchable\":true,\"sortable\":true}},\"mime\":{\"edit\":{\"label\":\"Mime\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Mime\",\"searchable\":true,\"sortable\":true}},\"size\":{\"edit\":{\"label\":\"Size\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Size\",\"searchable\":true,\"sortable\":true}},\"url\":{\"edit\":{\"label\":\"Url\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Url\",\"searchable\":true,\"sortable\":true}},\"previewUrl\":{\"edit\":{\"label\":\"PreviewUrl\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"PreviewUrl\",\"searchable\":true,\"sortable\":true}},\"provider\":{\"edit\":{\"label\":\"Provider\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Provider\",\"searchable\":true,\"sortable\":true}},\"provider_metadata\":{\"edit\":{\"label\":\"Provider_metadata\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Provider_metadata\",\"searchable\":false,\"sortable\":false}},\"related\":{\"edit\":{\"label\":\"Related\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"id\"},\"list\":{\"label\":\"Related\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"alternativeText\",\"caption\"],\"editRelations\":[\"related\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"alternativeText\",\"size\":6}],[{\"name\":\"caption\",\"size\":6},{\"name\":\"width\",\"size\":4}],[{\"name\":\"height\",\"size\":4}],[{\"name\":\"formats\",\"size\":12}],[{\"name\":\"hash\",\"size\":6},{\"name\":\"ext\",\"size\":6}],[{\"name\":\"mime\",\"size\":6},{\"name\":\"size\",\"size\":4}],[{\"name\":\"url\",\"size\":6},{\"name\":\"previewUrl\",\"size\":6}],[{\"name\":\"provider\",\"size\":6}],[{\"name\":\"provider_metadata\",\"size\":12}]]}}','object','',''),(15,'plugin_content_manager_configuration_content_types::strapi::administrator','{\"uid\":\"strapi::administrator\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"username\",\"defaultSortBy\":\"username\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"username\":{\"edit\":{\"label\":\"Username\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Username\",\"searchable\":true,\"sortable\":true}},\"email\":{\"edit\":{\"label\":\"Email\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Email\",\"searchable\":true,\"sortable\":true}},\"password\":{\"edit\":{\"label\":\"Password\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Password\",\"searchable\":true,\"sortable\":true}},\"resetPasswordToken\":{\"edit\":{\"label\":\"ResetPasswordToken\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"ResetPasswordToken\",\"searchable\":true,\"sortable\":true}},\"blocked\":{\"edit\":{\"label\":\"Blocked\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Blocked\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"username\",\"email\",\"blocked\"],\"editRelations\":[],\"edit\":[[{\"name\":\"username\",\"size\":6},{\"name\":\"email\",\"size\":6}],[{\"name\":\"password\",\"size\":6},{\"name\":\"blocked\",\"size\":4}]]}}','object','',''),(16,'plugin_content_manager_configuration_content_types::plugins::users-permissions.user','{\"uid\":\"plugins::users-permissions.user\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"username\",\"defaultSortBy\":\"username\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"username\":{\"edit\":{\"label\":\"Username\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Username\",\"searchable\":true,\"sortable\":true}},\"email\":{\"edit\":{\"label\":\"Email\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Email\",\"searchable\":true,\"sortable\":true}},\"provider\":{\"edit\":{\"label\":\"Provider\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Provider\",\"searchable\":true,\"sortable\":true}},\"password\":{\"edit\":{\"label\":\"Password\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Password\",\"searchable\":true,\"sortable\":true}},\"resetPasswordToken\":{\"edit\":{\"label\":\"ResetPasswordToken\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"ResetPasswordToken\",\"searchable\":true,\"sortable\":true}},\"confirmed\":{\"edit\":{\"label\":\"Confirmed\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Confirmed\",\"searchable\":true,\"sortable\":true}},\"blocked\":{\"edit\":{\"label\":\"Blocked\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Blocked\",\"searchable\":true,\"sortable\":true}},\"role\":{\"edit\":{\"label\":\"Role\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Role\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"username\",\"email\",\"confirmed\"],\"editRelations\":[\"role\"],\"edit\":[[{\"name\":\"username\",\"size\":6},{\"name\":\"email\",\"size\":6}],[{\"name\":\"password\",\"size\":6},{\"name\":\"confirmed\",\"size\":4}],[{\"name\":\"blocked\",\"size\":4}]]}}','object','',''),(17,'plugin_users-permissions_email','{\"reset_password\":{\"display\":\"Email.template.reset_password\",\"icon\":\"sync\",\"options\":{\"from\":{\"name\":\"Administration Panel\",\"email\":\"no-reply@strapi.io\"},\"response_email\":\"\",\"object\":\"Reset password\",\"message\":\"<p>We heard that you lost your password. Sorry about that!</p>\\n\\n<p>But don’t worry! You can use the following link to reset your password:</p>\\n\\n<p><%= URL %>?code=<%= TOKEN %></p>\\n\\n<p>Thanks.</p>\"}},\"email_confirmation\":{\"display\":\"Email.template.email_confirmation\",\"icon\":\"check-square\",\"options\":{\"from\":{\"name\":\"Administration Panel\",\"email\":\"no-reply@strapi.io\"},\"response_email\":\"\",\"object\":\"Account confirmation\",\"message\":\"<p>Thank you for registering!</p>\\n\\n<p>You have to confirm your email address. Please click on the link below.</p>\\n\\n<p><%= URL %>?confirmation=<%= CODE %></p>\\n\\n<p>Thanks.</p>\"}}}','object','',''),(18,'plugin_users-permissions_advanced','{\"unique_email\":true,\"allow_register\":true,\"email_confirmation\":false,\"email_confirmation_redirection\":\"http://0.0.0.0:1337/admin\",\"email_reset_password\":\"http://0.0.0.0:1337/admin\",\"default_role\":\"authenticated\"}','object','',''),(19,'db_model_about_uses','{\"title\":{\"type\":\"string\",\"required\":true},\"description\":{\"type\":\"richtext\",\"required\":true},\"picture\":{\"model\":\"file\",\"via\":\"related\",\"allowedTypes\":[\"images\",\"videos\"],\"plugin\":\"upload\",\"required\":false},\"created_at\":{\"type\":\"currentTimestamp\"},\"updated_at\":{\"type\":\"currentTimestamp\"}}','object',NULL,NULL),(20,'db_model_missions','{\"picture\":{\"model\":\"file\",\"via\":\"related\",\"allowedTypes\":[\"files\",\"images\",\"videos\"],\"plugin\":\"upload\",\"required\":false},\"i18n\":{\"type\":\"component\",\"repeatable\":true,\"component\":\"i18n.mission\"},\"created_at\":{\"type\":\"currentTimestamp\"},\"updated_at\":{\"type\":\"currentTimestamp\"}}','object',NULL,NULL),(21,'db_model_activities','{\"Pictures\":{\"collection\":\"file\",\"via\":\"related\",\"allowedTypes\":[\"images\",\"videos\"],\"plugin\":\"upload\",\"required\":false},\"Icon\":{\"type\":\"enumeration\",\"enum\":[\"leaves\",\"hourglass\",\"shoppingBags\",\"recycle\",\"homeHart\",\"laptop\",\"books\",\"web\",\"people\",\"peopleNetwork\"]},\"i18n\":{\"type\":\"component\",\"repeatable\":true,\"component\":\"i18n.activities-i18n\"},\"created_at\":{\"type\":\"currentTimestamp\"},\"updated_at\":{\"type\":\"currentTimestamp\"}}','object',NULL,NULL),(22,'plugin_content_manager_configuration_content_types::application::about-us.about-us','{\"uid\":\"application::about-us.about-us\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"title\",\"defaultSortBy\":\"title\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"title\":{\"edit\":{\"label\":\"Title\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Title\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"Description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Description\",\"searchable\":false,\"sortable\":false}},\"picture\":{\"edit\":{\"label\":\"Picture\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Picture\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"title\",\"picture\",\"created_at\"],\"editRelations\":[],\"edit\":[[{\"name\":\"title\",\"size\":6}],[{\"name\":\"description\",\"size\":12}],[{\"name\":\"picture\",\"size\":6}]]}}','object','',''),(23,'plugin_content_manager_configuration_content_types::application::activities.activities','{\"uid\":\"application::activities.activities\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"id\",\"defaultSortBy\":\"id\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"Pictures\":{\"edit\":{\"label\":\"Pictures\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Pictures\",\"searchable\":false,\"sortable\":false}},\"Icon\":{\"edit\":{\"label\":\"Icon\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Icon\",\"searchable\":true,\"sortable\":true}},\"i18n\":{\"edit\":{\"label\":\"I18n\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"I18n\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"Pictures\",\"Icon\"],\"edit\":[[{\"name\":\"i18n\",\"size\":12}],[{\"name\":\"Pictures\",\"size\":6},{\"name\":\"Icon\",\"size\":6}]],\"editRelations\":[]}}','object','',''),(24,'plugin_content_manager_configuration_content_types::application::mission.mission','{\"uid\":\"application::mission.mission\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"id\",\"defaultSortBy\":\"id\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"picture\":{\"edit\":{\"label\":\"Picture\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Picture\",\"searchable\":false,\"sortable\":false}},\"i18n\":{\"edit\":{\"label\":\"I18n\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"I18n\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"created_at\",\"picture\"],\"edit\":[[{\"name\":\"picture\",\"size\":6}],[{\"name\":\"i18n\",\"size\":12}]],\"editRelations\":[]}}','object','',''),(25,'db_model_components_page_content_heroes','{\"Slogan\":{\"type\":\"string\",\"required\":false},\"Picture\":{\"model\":\"file\",\"via\":\"related\",\"allowedTypes\":[\"files\",\"images\",\"videos\"],\"plugin\":\"upload\",\"required\":true}}','object',NULL,NULL),(26,'db_model_components_i18n_activities_i18ns','{\"title\":{\"type\":\"string\",\"required\":true},\"description\":{\"type\":\"richtext\"},\"language\":{\"type\":\"enumeration\",\"enum\":[\"en\",\"es\",\"ca\"]}}','object',NULL,NULL),(27,'db_model_components_page_content_media_objects','{\"Title\":{\"type\":\"string\",\"required\":true},\"Body\":{\"type\":\"richtext\"},\"Picture\":{\"model\":\"file\",\"via\":\"related\",\"allowedTypes\":[\"images\",\"videos\"],\"plugin\":\"upload\",\"required\":false},\"IconType\":{\"type\":\"enumeration\",\"enum\":[\"leaves\",\"hourglass\",\"bags\",\"recycle\",\"homeWithHart\",\"laptop\",\"books\",\"web\",\"people\",\"peopleNetwork\"]}}','object',NULL,NULL),(28,'plugin_content_manager_configuration_components::i18n.activities-i18n','{\"uid\":\"i18n.activities-i18n\",\"isComponent\":true,\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"title\",\"defaultSortBy\":\"title\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":false,\"sortable\":false}},\"title\":{\"edit\":{\"label\":\"Title\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Title\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"Description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Description\",\"searchable\":false,\"sortable\":false}},\"language\":{\"edit\":{\"label\":\"Language\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Language\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"title\",\"language\"],\"edit\":[[{\"name\":\"language\",\"size\":6}],[{\"name\":\"title\",\"size\":6}],[{\"name\":\"description\",\"size\":12}]],\"editRelations\":[]}}','object','',''),(30,'plugin_content_manager_configuration_components::page-content.media-object','{\"uid\":\"page-content.media-object\",\"isComponent\":true,\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"Title\",\"defaultSortBy\":\"Title\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":false,\"sortable\":false}},\"Title\":{\"edit\":{\"label\":\"Title\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Title\",\"searchable\":true,\"sortable\":true}},\"Body\":{\"edit\":{\"label\":\"Body\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Body\",\"searchable\":false,\"sortable\":false}},\"Picture\":{\"edit\":{\"label\":\"Picture\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Picture\",\"searchable\":false,\"sortable\":false}},\"IconType\":{\"edit\":{\"label\":\"IconType\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"IconType\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"Title\",\"Picture\",\"IconType\"],\"edit\":[[{\"name\":\"Title\",\"size\":6}],[{\"name\":\"Body\",\"size\":12}],[{\"name\":\"Picture\",\"size\":6},{\"name\":\"IconType\",\"size\":6}]],\"editRelations\":[]}}','object','',''),(31,'db_model_heroes','{\"en\":{\"type\":\"string\",\"required\":true},\"es\":{\"type\":\"string\",\"required\":true},\"ca\":{\"type\":\"string\"},\"picture\":{\"model\":\"file\",\"via\":\"related\",\"allowedTypes\":[\"images\",\"videos\"],\"plugin\":\"upload\",\"required\":false},\"created_at\":{\"type\":\"currentTimestamp\"},\"updated_at\":{\"type\":\"currentTimestamp\"}}','object',NULL,NULL),(32,'plugin_content_manager_configuration_content_types::application::hero.hero','{\"uid\":\"application::hero.hero\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"en\",\"defaultSortBy\":\"en\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"en\":{\"edit\":{\"label\":\"En\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"En\",\"searchable\":true,\"sortable\":true}},\"es\":{\"edit\":{\"label\":\"Es\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Es\",\"searchable\":true,\"sortable\":true}},\"ca\":{\"edit\":{\"label\":\"Ca\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Ca\",\"searchable\":true,\"sortable\":true}},\"picture\":{\"edit\":{\"label\":\"Picture\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Picture\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"en\",\"es\",\"ca\"],\"editRelations\":[],\"edit\":[[{\"name\":\"en\",\"size\":6},{\"name\":\"es\",\"size\":6}],[{\"name\":\"ca\",\"size\":6},{\"name\":\"picture\",\"size\":6}]]}}','object','',''),(33,'db_model_components_i18n_missions','{\"title\":{\"type\":\"string\",\"required\":true},\"description\":{\"type\":\"richtext\"},\"language\":{\"type\":\"enumeration\",\"enum\":[\"en\",\"es\",\"ca\"],\"required\":true,\"default\":\"es\"},\"subtitle\":{\"type\":\"string\",\"required\":true}}','object',NULL,NULL),(34,'plugin_content_manager_configuration_components::i18n.mission','{\"uid\":\"i18n.mission\",\"isComponent\":true,\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"title\",\"defaultSortBy\":\"title\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":false,\"sortable\":false}},\"title\":{\"edit\":{\"label\":\"Title\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Title\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"Description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Description\",\"searchable\":false,\"sortable\":false}},\"language\":{\"edit\":{\"label\":\"Language\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Language\",\"searchable\":true,\"sortable\":true}},\"subtitle\":{\"edit\":{\"label\":\"Subtitle\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Subtitle\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"title\",\"language\",\"subtitle\"],\"edit\":[[{\"name\":\"title\",\"size\":6}],[{\"name\":\"subtitle\",\"size\":6}],[{\"name\":\"description\",\"size\":12}],[{\"name\":\"language\",\"size\":6}]],\"editRelations\":[]}}','object','','');
/*!40000 ALTER TABLE `core_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `heroes`
--

DROP TABLE IF EXISTS `heroes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `heroes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `en` varchar(255) NOT NULL,
  `es` varchar(255) NOT NULL,
  `ca` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `heroes`
--

LOCK TABLES `heroes` WRITE;
/*!40000 ALTER TABLE `heroes` DISABLE KEYS */;
INSERT INTO `heroes` VALUES (1,'The new conscious economy','La nueva economìa consciente',NULL,'2020-05-23 06:11:49','2020-05-23 06:12:11');
/*!40000 ALTER TABLE `heroes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `missions`
--

DROP TABLE IF EXISTS `missions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `missions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `missions`
--

LOCK TABLES `missions` WRITE;
/*!40000 ALTER TABLE `missions` DISABLE KEYS */;
INSERT INTO `missions` VALUES (1,'2020-05-20 09:31:02','2020-05-23 06:29:26');
/*!40000 ALTER TABLE `missions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `missions_components`
--

DROP TABLE IF EXISTS `missions_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `missions_components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field` varchar(255) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `component_type` varchar(255) NOT NULL,
  `component_id` int(11) NOT NULL,
  `mission_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mission_id_fk` (`mission_id`),
  CONSTRAINT `mission_id_fk` FOREIGN KEY (`mission_id`) REFERENCES `missions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `missions_components`
--

LOCK TABLES `missions_components` WRITE;
/*!40000 ALTER TABLE `missions_components` DISABLE KEYS */;
INSERT INTO `missions_components` VALUES (1,'i18n',1,'components_i18n_missions',1,1),(2,'i18n',2,'components_i18n_missions',2,1);
/*!40000 ALTER TABLE `missions_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `strapi_administrator`
--

DROP TABLE IF EXISTS `strapi_administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `strapi_administrator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `resetPasswordToken` varchar(255) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `strapi_administrator_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `strapi_administrator`
--

LOCK TABLES `strapi_administrator` WRITE;
/*!40000 ALTER TABLE `strapi_administrator` DISABLE KEYS */;
INSERT INTO `strapi_administrator` VALUES (1,'ale','alessio.carnevale@gmail.com','$2a$10$lzrAEevfUFabF/mWth1L0OgAy.V6V.1tq7023uz/gop0sj6zGBQGq',NULL,NULL);
/*!40000 ALTER TABLE `strapi_administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `strapi_webhooks`
--

DROP TABLE IF EXISTS `strapi_webhooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `strapi_webhooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` longtext DEFAULT NULL,
  `headers` longtext DEFAULT NULL,
  `events` longtext DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `strapi_webhooks`
--

LOCK TABLES `strapi_webhooks` WRITE;
/*!40000 ALTER TABLE `strapi_webhooks` DISABLE KEYS */;
/*!40000 ALTER TABLE `strapi_webhooks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upload_file`
--

DROP TABLE IF EXISTS `upload_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alternativeText` varchar(255) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `formats` longtext DEFAULT NULL,
  `hash` varchar(255) NOT NULL,
  `ext` varchar(255) DEFAULT NULL,
  `mime` varchar(255) NOT NULL,
  `size` decimal(10,2) NOT NULL,
  `url` varchar(255) NOT NULL,
  `previewUrl` varchar(255) DEFAULT NULL,
  `provider` varchar(255) NOT NULL,
  `provider_metadata` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upload_file`
--

LOCK TABLES `upload_file` WRITE;
/*!40000 ALTER TABLE `upload_file` DISABLE KEYS */;
INSERT INTO `upload_file` VALUES (1,'rabie-madaci-skx1Rn6LW9I-unsplash','','',6000,4000,'{\"thumbnail\":{\"hash\":\"thumbnail_rabie-madaci-skx1Rn6LW9I-unsplash_f1308df80e\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":234,\"height\":156,\"size\":7.01,\"path\":null,\"url\":\"/uploads/thumbnail_rabie-madaci-skx1Rn6LW9I-unsplash_f1308df80e.jpeg\"},\"large\":{\"hash\":\"large_rabie-madaci-skx1Rn6LW9I-unsplash_f1308df80e\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":1000,\"height\":667,\"size\":85.28,\"path\":null,\"url\":\"/uploads/large_rabie-madaci-skx1Rn6LW9I-unsplash_f1308df80e.jpeg\"},\"medium\":{\"hash\":\"medium_rabie-madaci-skx1Rn6LW9I-unsplash_f1308df80e\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":750,\"height\":500,\"size\":49.47,\"path\":null,\"url\":\"/uploads/medium_rabie-madaci-skx1Rn6LW9I-unsplash_f1308df80e.jpeg\"},\"small\":{\"hash\":\"small_rabie-madaci-skx1Rn6LW9I-unsplash_f1308df80e\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":500,\"height\":333,\"size\":24.46,\"path\":null,\"url\":\"/uploads/small_rabie-madaci-skx1Rn6LW9I-unsplash_f1308df80e.jpeg\"}}','rabie-madaci-skx1Rn6LW9I-unsplash_f1308df80e','.jpeg','image/jpeg',3886.41,'/uploads/rabie-madaci-skx1Rn6LW9I-unsplash_f1308df80e.jpeg',NULL,'local',NULL,'2020-05-20 09:35:25','2020-05-20 09:35:25'),(2,'helena-lopes-PGnqT0rXWLs-unsplash','','',5472,3648,'{\"thumbnail\":{\"hash\":\"thumbnail_helena-lopes-PGnqT0rXWLs-unsplash_1e7fc8210a\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":234,\"height\":156,\"size\":6.99,\"path\":null,\"url\":\"/uploads/thumbnail_helena-lopes-PGnqT0rXWLs-unsplash_1e7fc8210a.jpeg\"},\"large\":{\"hash\":\"large_helena-lopes-PGnqT0rXWLs-unsplash_1e7fc8210a\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":1000,\"height\":667,\"size\":113.55,\"path\":null,\"url\":\"/uploads/large_helena-lopes-PGnqT0rXWLs-unsplash_1e7fc8210a.jpeg\"},\"medium\":{\"hash\":\"medium_helena-lopes-PGnqT0rXWLs-unsplash_1e7fc8210a\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":750,\"height\":500,\"size\":63.1,\"path\":null,\"url\":\"/uploads/medium_helena-lopes-PGnqT0rXWLs-unsplash_1e7fc8210a.jpeg\"},\"small\":{\"hash\":\"small_helena-lopes-PGnqT0rXWLs-unsplash_1e7fc8210a\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":500,\"height\":333,\"size\":27.47,\"path\":null,\"url\":\"/uploads/small_helena-lopes-PGnqT0rXWLs-unsplash_1e7fc8210a.jpeg\"}}','helena-lopes-PGnqT0rXWLs-unsplash_1e7fc8210a','.jpeg','image/jpeg',3781.15,'/uploads/helena-lopes-PGnqT0rXWLs-unsplash_1e7fc8210a.jpeg',NULL,'local',NULL,'2020-05-20 09:40:17','2020-05-20 09:40:17'),(3,'cobhouse','','',1479,990,'{\"thumbnail\":{\"hash\":\"thumbnail_cobhouse_4af67f2ed8\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":233,\"height\":156,\"size\":10.95,\"path\":null,\"url\":\"/uploads/thumbnail_cobhouse_4af67f2ed8.jpeg\"},\"large\":{\"hash\":\"large_cobhouse_4af67f2ed8\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":1000,\"height\":669,\"size\":164.3,\"path\":null,\"url\":\"/uploads/large_cobhouse_4af67f2ed8.jpeg\"},\"medium\":{\"hash\":\"medium_cobhouse_4af67f2ed8\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":750,\"height\":502,\"size\":92.83,\"path\":null,\"url\":\"/uploads/medium_cobhouse_4af67f2ed8.jpeg\"},\"small\":{\"hash\":\"small_cobhouse_4af67f2ed8\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":500,\"height\":335,\"size\":41.49,\"path\":null,\"url\":\"/uploads/small_cobhouse_4af67f2ed8.jpeg\"}}','cobhouse_4af67f2ed8','.jpeg','image/jpeg',291.45,'/uploads/cobhouse_4af67f2ed8.jpeg',NULL,'local',NULL,'2020-05-20 11:35:13','2020-05-20 11:35:13'),(4,'chang-duong-Sj0iMtq_Z4w-unsplash','','',1920,1280,'{\"thumbnail\":{\"hash\":\"thumbnail_chang-duong-Sj0iMtq_Z4w-unsplash_5fd296a514\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":234,\"height\":156,\"size\":4.85,\"path\":null,\"url\":\"/uploads/thumbnail_chang-duong-Sj0iMtq_Z4w-unsplash_5fd296a514.jpeg\"},\"large\":{\"hash\":\"large_chang-duong-Sj0iMtq_Z4w-unsplash_5fd296a514\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":1000,\"height\":667,\"size\":52.54,\"path\":null,\"url\":\"/uploads/large_chang-duong-Sj0iMtq_Z4w-unsplash_5fd296a514.jpeg\"},\"medium\":{\"hash\":\"medium_chang-duong-Sj0iMtq_Z4w-unsplash_5fd296a514\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":750,\"height\":500,\"size\":29.91,\"path\":null,\"url\":\"/uploads/medium_chang-duong-Sj0iMtq_Z4w-unsplash_5fd296a514.jpeg\"},\"small\":{\"hash\":\"small_chang-duong-Sj0iMtq_Z4w-unsplash_5fd296a514\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":500,\"height\":333,\"size\":15.25,\"path\":null,\"url\":\"/uploads/small_chang-duong-Sj0iMtq_Z4w-unsplash_5fd296a514.jpeg\"}}','chang-duong-Sj0iMtq_Z4w-unsplash_5fd296a514','.jpeg','image/jpeg',235.81,'/uploads/chang-duong-Sj0iMtq_Z4w-unsplash_5fd296a514.jpeg',NULL,'local',NULL,'2020-05-20 11:35:13','2020-05-20 11:35:13'),(5,'tools','','',1920,1440,'{\"thumbnail\":{\"hash\":\"thumbnail_tools_a34b8dd3e9\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":208,\"height\":156,\"size\":9.35,\"path\":null,\"url\":\"/uploads/thumbnail_tools_a34b8dd3e9.jpeg\"},\"large\":{\"hash\":\"large_tools_a34b8dd3e9\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":1000,\"height\":750,\"size\":139.93,\"path\":null,\"url\":\"/uploads/large_tools_a34b8dd3e9.jpeg\"},\"medium\":{\"hash\":\"medium_tools_a34b8dd3e9\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":750,\"height\":563,\"size\":82.7,\"path\":null,\"url\":\"/uploads/medium_tools_a34b8dd3e9.jpeg\"},\"small\":{\"hash\":\"small_tools_a34b8dd3e9\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":500,\"height\":375,\"size\":41.54,\"path\":null,\"url\":\"/uploads/small_tools_a34b8dd3e9.jpeg\"}}','tools_a34b8dd3e9','.jpeg','image/jpeg',438.76,'/uploads/tools_a34b8dd3e9.jpeg',NULL,'local',NULL,'2020-05-20 11:35:13','2020-05-20 11:35:13'),(6,'hermes-rivera-0h72pYvHrEY-unsplash','','',6000,3376,'{\"thumbnail\":{\"hash\":\"thumbnail_hermes-rivera-0h72pYvHrEY-unsplash_806c83be04\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":245,\"height\":138,\"size\":11.55,\"path\":null,\"url\":\"/uploads/thumbnail_hermes-rivera-0h72pYvHrEY-unsplash_806c83be04.jpeg\"},\"large\":{\"hash\":\"large_hermes-rivera-0h72pYvHrEY-unsplash_806c83be04\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":1000,\"height\":563,\"size\":92.04,\"path\":null,\"url\":\"/uploads/large_hermes-rivera-0h72pYvHrEY-unsplash_806c83be04.jpeg\"},\"medium\":{\"hash\":\"medium_hermes-rivera-0h72pYvHrEY-unsplash_806c83be04\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":750,\"height\":422,\"size\":59.67,\"path\":null,\"url\":\"/uploads/medium_hermes-rivera-0h72pYvHrEY-unsplash_806c83be04.jpeg\"},\"small\":{\"hash\":\"small_hermes-rivera-0h72pYvHrEY-unsplash_806c83be04\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":500,\"height\":281,\"size\":32.75,\"path\":null,\"url\":\"/uploads/small_hermes-rivera-0h72pYvHrEY-unsplash_806c83be04.jpeg\"}}','hermes-rivera-0h72pYvHrEY-unsplash_806c83be04','.jpeg','image/jpeg',1681.91,'/uploads/hermes-rivera-0h72pYvHrEY-unsplash_806c83be04.jpeg',NULL,'local',NULL,'2020-05-20 11:35:14','2020-05-20 11:35:14'),(7,'hannah-busing-Zyx1bK9mqmA-unsplash','','',5040,3360,'{\"thumbnail\":{\"hash\":\"thumbnail_hannah-busing-Zyx1bK9mqmA-unsplash_579997ca7a\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":234,\"height\":156,\"size\":8.93,\"path\":null,\"url\":\"/uploads/thumbnail_hannah-busing-Zyx1bK9mqmA-unsplash_579997ca7a.jpeg\"},\"large\":{\"hash\":\"large_hannah-busing-Zyx1bK9mqmA-unsplash_579997ca7a\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":1000,\"height\":667,\"size\":89.9,\"path\":null,\"url\":\"/uploads/large_hannah-busing-Zyx1bK9mqmA-unsplash_579997ca7a.jpeg\"},\"medium\":{\"hash\":\"medium_hannah-busing-Zyx1bK9mqmA-unsplash_579997ca7a\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":750,\"height\":500,\"size\":56.35,\"path\":null,\"url\":\"/uploads/medium_hannah-busing-Zyx1bK9mqmA-unsplash_579997ca7a.jpeg\"},\"small\":{\"hash\":\"small_hannah-busing-Zyx1bK9mqmA-unsplash_579997ca7a\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":500,\"height\":333,\"size\":29.09,\"path\":null,\"url\":\"/uploads/small_hannah-busing-Zyx1bK9mqmA-unsplash_579997ca7a.jpeg\"}}','hannah-busing-Zyx1bK9mqmA-unsplash_579997ca7a','.jpeg','image/jpeg',2915.69,'/uploads/hannah-busing-Zyx1bK9mqmA-unsplash_579997ca7a.jpeg',NULL,'local',NULL,'2020-05-20 11:35:14','2020-05-20 11:35:14'),(8,'rabie-madaci-skx1Rn6LW9I-unsplash','','',6000,4000,'{\"thumbnail\":{\"hash\":\"thumbnail_rabie-madaci-skx1Rn6LW9I-unsplash_3d5f1b5bd3\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":234,\"height\":156,\"size\":7.01,\"path\":null,\"url\":\"/uploads/thumbnail_rabie-madaci-skx1Rn6LW9I-unsplash_3d5f1b5bd3.jpeg\"},\"large\":{\"hash\":\"large_rabie-madaci-skx1Rn6LW9I-unsplash_3d5f1b5bd3\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":1000,\"height\":667,\"size\":85.28,\"path\":null,\"url\":\"/uploads/large_rabie-madaci-skx1Rn6LW9I-unsplash_3d5f1b5bd3.jpeg\"},\"medium\":{\"hash\":\"medium_rabie-madaci-skx1Rn6LW9I-unsplash_3d5f1b5bd3\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":750,\"height\":500,\"size\":49.47,\"path\":null,\"url\":\"/uploads/medium_rabie-madaci-skx1Rn6LW9I-unsplash_3d5f1b5bd3.jpeg\"},\"small\":{\"hash\":\"small_rabie-madaci-skx1Rn6LW9I-unsplash_3d5f1b5bd3\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":500,\"height\":333,\"size\":24.46,\"path\":null,\"url\":\"/uploads/small_rabie-madaci-skx1Rn6LW9I-unsplash_3d5f1b5bd3.jpeg\"}}','rabie-madaci-skx1Rn6LW9I-unsplash_3d5f1b5bd3','.jpeg','image/jpeg',3886.41,'/uploads/rabie-madaci-skx1Rn6LW9I-unsplash_3d5f1b5bd3.jpeg',NULL,'local',NULL,'2020-05-20 11:35:14','2020-05-20 11:35:14'),(9,'chang-duong-Sj0iMtq_Z4w-unsplash','','',600,600,'{\"thumbnail\":{\"hash\":\"thumbnail_chang-duong-Sj0iMtq_Z4w-unsplash_9d6b9fd94d\",\"ext\":\".png\",\"mime\":\"image/png\",\"width\":156,\"height\":156,\"size\":41.05,\"path\":null,\"url\":\"/uploads/thumbnail_chang-duong-Sj0iMtq_Z4w-unsplash_9d6b9fd94d.png\"},\"small\":{\"hash\":\"small_chang-duong-Sj0iMtq_Z4w-unsplash_9d6b9fd94d\",\"ext\":\".png\",\"mime\":\"image/png\",\"width\":500,\"height\":500,\"size\":297.04,\"path\":null,\"url\":\"/uploads/small_chang-duong-Sj0iMtq_Z4w-unsplash_9d6b9fd94d.png\"}}','chang-duong-Sj0iMtq_Z4w-unsplash_9d6b9fd94d','.png','image/png',371.51,'/uploads/chang-duong-Sj0iMtq_Z4w-unsplash_9d6b9fd94d.png',NULL,'local',NULL,'2020-05-20 13:17:06','2020-05-20 13:17:06'),(10,'leavesBg','','',2400,1600,'{\"thumbnail\":{\"hash\":\"thumbnail_leavesBg_475d922533\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":234,\"height\":156,\"size\":7.81,\"path\":null,\"url\":\"/uploads/thumbnail_leavesBg_475d922533.jpeg\"},\"large\":{\"hash\":\"large_leavesBg_475d922533\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":1000,\"height\":667,\"size\":69.46,\"path\":null,\"url\":\"/uploads/large_leavesBg_475d922533.jpeg\"},\"medium\":{\"hash\":\"medium_leavesBg_475d922533\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":750,\"height\":500,\"size\":42.3,\"path\":null,\"url\":\"/uploads/medium_leavesBg_475d922533.jpeg\"},\"small\":{\"hash\":\"small_leavesBg_475d922533\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":500,\"height\":333,\"size\":22.75,\"path\":null,\"url\":\"/uploads/small_leavesBg_475d922533.jpeg\"}}','leavesBg_475d922533','.jpeg','image/jpeg',527.77,'/uploads/leavesBg_475d922533.jpeg',NULL,'local',NULL,'2020-05-23 06:11:44','2020-05-23 06:11:44');
/*!40000 ALTER TABLE `upload_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upload_file_morph`
--

DROP TABLE IF EXISTS `upload_file_morph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload_file_morph` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `upload_file_id` int(11) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL,
  `related_type` longtext DEFAULT NULL,
  `field` longtext DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upload_file_morph`
--

LOCK TABLES `upload_file_morph` WRITE;
/*!40000 ALTER TABLE `upload_file_morph` DISABLE KEYS */;
INSERT INTO `upload_file_morph` VALUES (8,7,2,'activities','Pictures',1),(10,1,4,'activities','Pictures',1),(11,5,5,'activities','Pictures',1),(15,10,1,'heroes','picture',1),(19,1,1,'activities','Pictures',1),(23,9,1,'missions','picture',1),(24,3,3,'activities','Pictures',1);
/*!40000 ALTER TABLE `upload_file_morph` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users-permissions_permission`
--

DROP TABLE IF EXISTS `users-permissions_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users-permissions_permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `policy` varchar(255) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users-permissions_permission`
--

LOCK TABLES `users-permissions_permission` WRITE;
/*!40000 ALTER TABLE `users-permissions_permission` DISABLE KEYS */;
INSERT INTO `users-permissions_permission` VALUES (1,'content-manager','components','findcomponent',0,'',1),(2,'content-manager','components','findcomponent',0,'',2),(3,'content-manager','components','listcomponents',0,'',1),(4,'content-manager','components','listcomponents',0,'',2),(5,'content-manager','components','updatecomponent',0,'',1),(6,'content-manager','components','updatecomponent',0,'',2),(7,'content-manager','contentmanager','checkuidavailability',0,'',1),(8,'content-manager','contentmanager','checkuidavailability',0,'',2),(9,'content-manager','contentmanager','count',0,'',1),(10,'content-manager','contentmanager','count',0,'',2),(11,'content-manager','contentmanager','create',0,'',1),(12,'content-manager','contentmanager','create',0,'',2),(13,'content-manager','contentmanager','delete',0,'',1),(14,'content-manager','contentmanager','delete',0,'',2),(15,'content-manager','contentmanager','deletemany',0,'',1),(16,'content-manager','contentmanager','deletemany',0,'',2),(17,'content-manager','contentmanager','find',0,'',1),(18,'content-manager','contentmanager','find',0,'',2),(19,'content-manager','contentmanager','findone',0,'',1),(20,'content-manager','contentmanager','findone',0,'',2),(21,'content-manager','contentmanager','generateuid',0,'',1),(22,'content-manager','contentmanager','generateuid',0,'',2),(23,'content-manager','contentmanager','update',0,'',1),(24,'content-manager','contentmanager','update',0,'',2),(25,'content-manager','contenttypes','findcontenttype',0,'',1),(26,'content-manager','contenttypes','findcontenttype',0,'',2),(27,'content-manager','contenttypes','listcontenttypes',0,'',1),(28,'content-manager','contenttypes','listcontenttypes',0,'',2),(29,'content-manager','contenttypes','updatecontenttype',0,'',1),(30,'content-manager','contenttypes','updatecontenttype',0,'',2),(31,'content-type-builder','componentcategories','deletecategory',0,'',1),(32,'content-type-builder','componentcategories','deletecategory',0,'',2),(33,'content-type-builder','componentcategories','editcategory',0,'',1),(34,'content-type-builder','componentcategories','editcategory',0,'',2),(35,'content-type-builder','components','createcomponent',0,'',1),(36,'content-type-builder','components','createcomponent',0,'',2),(37,'content-type-builder','components','deletecomponent',0,'',1),(38,'content-type-builder','components','deletecomponent',0,'',2),(39,'content-type-builder','components','getcomponent',0,'',1),(40,'content-type-builder','components','getcomponent',0,'',2),(41,'content-type-builder','components','getcomponents',0,'',1),(42,'content-type-builder','components','getcomponents',0,'',2),(43,'content-type-builder','components','updatecomponent',0,'',1),(44,'content-type-builder','components','updatecomponent',0,'',2),(45,'content-type-builder','connections','getconnections',0,'',1),(46,'content-type-builder','connections','getconnections',0,'',2),(47,'content-type-builder','contenttypes','createcontenttype',0,'',1),(48,'content-type-builder','contenttypes','createcontenttype',0,'',2),(49,'content-type-builder','contenttypes','deletecontenttype',0,'',1),(50,'content-type-builder','contenttypes','deletecontenttype',0,'',2),(51,'content-type-builder','contenttypes','getcontenttype',0,'',1),(52,'content-type-builder','contenttypes','getcontenttype',0,'',2),(53,'content-type-builder','contenttypes','getcontenttypes',0,'',1),(54,'content-type-builder','contenttypes','getcontenttypes',0,'',2),(55,'content-type-builder','contenttypes','updatecontenttype',0,'',1),(56,'content-type-builder','contenttypes','updatecontenttype',0,'',2),(57,'email','email','getenvironments',0,'',1),(58,'email','email','getenvironments',0,'',2),(59,'email','email','getsettings',0,'',1),(60,'email','email','getsettings',0,'',2),(61,'email','email','send',0,'',1),(62,'email','email','send',0,'',2),(63,'email','email','updatesettings',0,'',1),(64,'email','email','updatesettings',0,'',2),(65,'upload','proxy','uploadproxy',0,'',1),(66,'upload','proxy','uploadproxy',0,'',2),(67,'upload','upload','count',0,'',1),(68,'upload','upload','count',0,'',2),(69,'upload','upload','destroy',0,'',1),(70,'upload','upload','destroy',0,'',2),(71,'upload','upload','find',0,'',1),(72,'upload','upload','find',0,'',2),(73,'upload','upload','findone',0,'',1),(74,'upload','upload','findone',0,'',2),(75,'upload','upload','getsettings',0,'',1),(76,'upload','upload','getsettings',0,'',2),(77,'upload','upload','search',0,'',1),(78,'upload','upload','search',0,'',2),(79,'upload','upload','updatesettings',0,'',1),(80,'upload','upload','updatesettings',0,'',2),(81,'upload','upload','upload',0,'',1),(82,'upload','upload','upload',0,'',2),(83,'users-permissions','auth','callback',0,'',1),(84,'users-permissions','auth','callback',1,'',2),(85,'users-permissions','auth','changepassword',0,'',1),(86,'users-permissions','auth','changepassword',1,'',2),(87,'users-permissions','auth','connect',1,'',1),(88,'users-permissions','auth','connect',1,'',2),(89,'users-permissions','auth','emailconfirmation',0,'',1),(90,'users-permissions','auth','emailconfirmation',1,'',2),(91,'users-permissions','auth','forgotpassword',0,'',1),(92,'users-permissions','auth','forgotpassword',1,'',2),(93,'users-permissions','auth','register',0,'',1),(94,'users-permissions','auth','register',1,'',2),(95,'users-permissions','auth','sendemailconfirmation',0,'',1),(96,'users-permissions','auth','sendemailconfirmation',0,'',2),(97,'users-permissions','user','create',0,'',1),(98,'users-permissions','user','create',0,'',2),(99,'users-permissions','user','destroy',0,'',1),(100,'users-permissions','user','destroy',0,'',2),(101,'users-permissions','user','destroyall',0,'',1),(102,'users-permissions','user','destroyall',0,'',2),(103,'users-permissions','user','find',0,'',1),(104,'users-permissions','user','find',0,'',2),(105,'users-permissions','user','findone',0,'',1),(106,'users-permissions','user','findone',0,'',2),(107,'users-permissions','user','me',1,'',1),(108,'users-permissions','user','me',1,'',2),(109,'users-permissions','user','update',0,'',1),(110,'users-permissions','user','update',0,'',2),(111,'users-permissions','userspermissions','createrole',0,'',1),(112,'users-permissions','userspermissions','createrole',0,'',2),(113,'users-permissions','userspermissions','deleteprovider',0,'',1),(114,'users-permissions','userspermissions','deleteprovider',0,'',2),(115,'users-permissions','userspermissions','deleterole',0,'',1),(116,'users-permissions','userspermissions','deleterole',0,'',2),(117,'users-permissions','userspermissions','getadvancedsettings',0,'',1),(118,'users-permissions','userspermissions','getadvancedsettings',0,'',2),(119,'users-permissions','userspermissions','getemailtemplate',0,'',1),(120,'users-permissions','userspermissions','getemailtemplate',0,'',2),(121,'users-permissions','userspermissions','getpermissions',0,'',1),(122,'users-permissions','userspermissions','getpermissions',0,'',2),(123,'users-permissions','userspermissions','getpolicies',0,'',1),(124,'users-permissions','userspermissions','getpolicies',0,'',2),(125,'users-permissions','userspermissions','getproviders',0,'',1),(126,'users-permissions','userspermissions','getproviders',0,'',2),(127,'users-permissions','userspermissions','getrole',0,'',1),(128,'users-permissions','userspermissions','getrole',0,'',2),(129,'users-permissions','userspermissions','getroles',0,'',1),(130,'users-permissions','userspermissions','getroles',0,'',2),(131,'users-permissions','userspermissions','getroutes',0,'',1),(132,'users-permissions','userspermissions','getroutes',0,'',2),(133,'users-permissions','userspermissions','index',0,'',1),(134,'users-permissions','userspermissions','index',0,'',2),(135,'users-permissions','userspermissions','init',1,'',1),(136,'users-permissions','userspermissions','init',1,'',2),(137,'users-permissions','userspermissions','searchusers',0,'',1),(138,'users-permissions','userspermissions','searchusers',0,'',2),(139,'users-permissions','userspermissions','updateadvancedsettings',0,'',1),(140,'users-permissions','userspermissions','updateadvancedsettings',0,'',2),(141,'users-permissions','userspermissions','updateemailtemplate',0,'',1),(142,'users-permissions','userspermissions','updateemailtemplate',0,'',2),(143,'users-permissions','userspermissions','updateproviders',0,'',1),(144,'users-permissions','userspermissions','updateproviders',0,'',2),(145,'users-permissions','userspermissions','updaterole',0,'',1),(146,'users-permissions','userspermissions','updaterole',0,'',2),(147,'application','about-us','delete',0,'',1),(148,'application','about-us','delete',0,'',2),(149,'application','about-us','find',0,'',1),(150,'application','about-us','find',1,'',2),(151,'application','about-us','update',0,'',1),(152,'application','about-us','update',0,'',2),(153,'application','activities','count',0,'',1),(154,'application','activities','count',0,'',2),(155,'application','activities','create',0,'',1),(156,'application','activities','create',0,'',2),(157,'application','activities','delete',0,'',1),(158,'application','activities','delete',0,'',2),(159,'application','activities','find',0,'',1),(160,'application','activities','find',1,'',2),(161,'application','activities','findone',0,'',1),(162,'application','activities','findone',1,'',2),(163,'application','activities','update',0,'',1),(164,'application','activities','update',0,'',2),(165,'application','mission','delete',0,'',1),(166,'application','mission','delete',0,'',2),(167,'application','mission','find',0,'',1),(168,'application','mission','find',1,'',2),(169,'application','mission','update',0,'',1),(170,'application','mission','update',0,'',2),(171,'application','hero','delete',0,'',1),(172,'application','hero','delete',0,'',2),(173,'application','hero','find',0,'',1),(174,'application','hero','find',1,'',2),(175,'application','hero','update',0,'',1),(176,'application','hero','update',0,'',2);
/*!40000 ALTER TABLE `users-permissions_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users-permissions_role`
--

DROP TABLE IF EXISTS `users-permissions_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users-permissions_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users-permissions_role_type_unique` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users-permissions_role`
--

LOCK TABLES `users-permissions_role` WRITE;
/*!40000 ALTER TABLE `users-permissions_role` DISABLE KEYS */;
INSERT INTO `users-permissions_role` VALUES (1,'Authenticated','Default role given to authenticated user.','authenticated'),(2,'Public','Default role given to unauthenticated user.','public');
/*!40000 ALTER TABLE `users-permissions_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users-permissions_user`
--

DROP TABLE IF EXISTS `users-permissions_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users-permissions_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `resetPasswordToken` varchar(255) DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `users-permissions_user_username_unique` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users-permissions_user`
--

LOCK TABLES `users-permissions_user` WRITE;
/*!40000 ALTER TABLE `users-permissions_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `users-permissions_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-23 14:47:22

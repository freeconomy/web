import React from "react";
import styled from "styled-components";

const P = styled.p`
  .more {
    display: inline-block;
    padding: 1px 5px;
    background-color: rgba(0, 0, 0, 0.3);
    color: white;
    font-size: 0.7em;
    font-weight: 400;
    border-radius: 20px;
    margin-left: 0.3em;
  }
`;
const Component = ({ text, maxLength, className }) => {
  const showEllipsis = text.length > maxLength;

  return (
    <P className={className}>
      {text.slice(0, maxLength)}
      {showEllipsis && (
        <>
          <span className="more">...more</span>
        </>
      )}
    </P>
  );
};

Component.displayName = "Ellipsis";
export default Component;

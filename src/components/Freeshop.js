import React from "react";
import styled from "styled-components";
import FreeshopPost from "components/FreeshopPost";
import FreeshopPage from 'components/pages/Freeshop';

import PostModal from 'components/modals/ShowPost';
import {  useParams, useHistory } from "react-router-dom";

const Wrapper = styled.div`
  background-color: #f0f0f0;
  padding: 10px;
  display: flex;
  flex-direction: column;
  ${FreeshopPost}:last-child {
    margin-bottom: 0px;
  }
`;
const Component = ({ className, posts }) => {
  const params = useParams()
  const history = useHistory();
  const showPost = posts && posts.find(p=>p.id===params.postId);

  const classes = ["Freeshop"];
  className && classes.push(className);

  function onClick(post){
    history.push(FreeshopPage.path(post.id))
  }

  function closeModal(){
    history.push(FreeshopPage.path())
  }

  return (
    <Wrapper className={classes.join(" ")}>
      {showPost && <PostModal post={showPost} onClose={closeModal} />}
      {posts && posts.map(e => <FreeshopPost key={e.id} post={e} onClick = {onClick} />)}
    </Wrapper>
  );
};

Component.displayName = "Freeshop";

export default Component;

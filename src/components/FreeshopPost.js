import React from 'react';
import styled from 'styled-components';
import moment from "moment";
import "moment/locale/es";
import "moment/locale/ca";
import useI18n from "hooks/i18n";
import MessagesCount from "components/post/MessagesCount";

import PostHeader from 'components/post/Header';
import PostPictures from 'components/post/Pictures';
import PostDetails from 'components/post/Details';



const Wrapper = styled.button`
  background-color: #fff;
  border-radius: 3px;
  border: 1px solid rgba(0, 0, 0, 0.1);
  cursor: pointer;
  font-family: inherit;
  font-size: inherit;
  font-weight: 200;
  line-height: inherit;
  margin-bottom: 10px;
  padding: 10px;
  text-align: left;

  .stats {
    display: flex;
    font-size: 1.4em;
    flex-direction: column;
    align-items: flex-end;
  }
`;


const Component= ({className,post, onClick}) => {
  const loc = useI18n();
  moment.locale(loc);
  
  if(!post) return null;
  const classes=['FreeshopPost']
  className && classes.push(className)

  function handleClick(){
    onClick(post)
  }

  return (
    <Wrapper onClick={handleClick} className={classes.join(' ')} key={post.id}>
      
      <PostHeader title = {post.title} updatedAt={moment(post.updated_at).format("DD MMM")}  />
      <PostPictures pictures={post.pictures} />
      <PostDetails owner={post.owner} description={post.description} maxLength={75}/>   

      <div className="stats">
        <MessagesCount count={post.messages.length} />
      </div>
    </Wrapper>      
  )
}

Component.displayName='FreeshopPost'

export default Component
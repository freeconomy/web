import React from "react";
import ReactDOMServer from "react-dom/server";
import styled from "styled-components";
import "leaflet/dist/leaflet.css";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import L from "leaflet";
import iconsLibrary from "../lib/iconsLibrary";
import {debounce} from 'lib/helpers'

const Wrapper = styled.div`
  height: 100%;
  .leaflet-container {
    min-height: 50px;
    height: 100%;
    position: relative;
    z-index: 1;
  }

  .leaflet-tile-container {
    filter: grayscale(0.5);
  }

  .map_activityIconMarker {
    border-radius: 30px;
    box-shadow: 0px 1px 3px 2px rgba(0, 0, 0, 0.5);
    border: 2px solid white;

    .map_iconWrapper {
      display:flex;
      align-items: center;
      justify-content: center;
      height: 100%;
      background: #6594da;
      border-radius: 30px;
    }

    .offer {
      background-color:#f07e0e;
    }
    img {
      width: 100%;
    }
    svg {
      width: 60%;
      fill: white;
    }
  }

`;

const center = {
  lat: 39.6138,
  lng: 2.9117
};

const zoom = 13;

// const url = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
const url = "https://tile-c.openstreetmap.fr/hot/{z}/{x}/{y}.png";
const attribution =
  '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors';

const tileLayerOptions = {
  url,
  attribution,
  maxNativeZoom: 15, // OSM max available zoom is at 19.
  maxZoom: 15 // Match the map maxZoom, or leave map.options.maxZoom undefined.
};

const mapIcons = Object.keys(iconsLibrary).reduce((accumulator, value) => {
  const Icon = iconsLibrary[value]

  accumulator[value] = L.divIcon({
    className: `map_activityIconMarker`,
    iconSize: [40, 40],
    iconAnchor: [20, 20],
    html: `
      <div class="map_iconWrapper ${value}">
        ${typeof Icon === 'string' ? 
          `<img src="${iconsLibrary[value]}" alt="${value}" />`
        :
        ReactDOMServer.renderToString(<Icon />)
      }
      </div>
    `
  });

  return accumulator;
}, {});

const Component = ({ className, markers, onMarkerClick, maxZoomLevel=19 }) => {
  const classes = ["MallorcaMap"];
  const mapRef = React.useRef();
  const mapWrapperRef = React.useRef();
  className && classes.push(className);
  
  function handleClick(m) {
    return () => {
      onMarkerClick(m);
    };
  }

  function resizeCallback() {
    if (mapRef.current && mapRef.current.leafletElement) {
      console.log("resizing!")
      mapRef.current.leafletElement.invalidateSize(true);
    }
  }

  React.useEffect(() => {
    const mapEl=mapWrapperRef.current

    const observer = new window.ResizeObserver(debounce(resizeCallback, 100));
    observer.observe(mapEl);

    return () => {
      observer.unobserve(mapEl);
    };
  }, []);  

  const bounds = markers.length
    ? new L.LatLngBounds(markers.map(m => [m.loc.lat, m.loc.lng]))
    : undefined;

  return (
    <Wrapper className={classes.join(" ")} ref={mapWrapperRef}>
      <Map
        ref={mapRef}
        center={[center.lat, center.lng]}
        zoom={zoom}
        bounds={bounds}
        boundsOptions={{ padding: [10, 10] }}
        useFlyTo={true}
      >
        <TileLayer {...{...tileLayerOptions, maxNativeZoom:maxZoomLevel, maxZoom: maxZoomLevel}} />
        {markers.map(m => {
          return (
            <Marker
              key={`${m.loc.lat}${m.loc.lng}`}
              icon={mapIcons[m.type]}
              title={m.title}
              position={[m.loc.lat, m.loc.lng]}
              onClick={handleClick(m)}
            >
              {m.popup && <Popup>{m.popup}</Popup>}
            </Marker>
          );
        })}
      </Map>
    </Wrapper>
  );
};

Component.displayName = "MallorcaMap";

export default Component;

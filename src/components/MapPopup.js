import styled from 'styled-components';

export default styled.div`
  text-align: center;
  width: 150px;
  a {
    display: flex;
    align-items: center;
    padding: 5px;
    margin-bottom: 5px;
    border-radius: 5px;
    background-color: #efefef;

    svg {
      width: 24px;
      height: 24px;
      margin-right: 5px;
    }
  }
`;
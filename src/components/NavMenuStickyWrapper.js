import React from 'react';
import styled from 'styled-components';
import TopBar from './TopBar';
import useMQ, {menuHeight} from "hooks/mediaQuery";

const Wrapper = styled.div`
  position: sticky;  
  top: ${({mq})=>menuHeight[mq]}px;
  z-index:9;

  .contentWrapper {
    width: 100%;
    left: 0;
    position: absolute;
    bottom: 0px;

    .TopBar {
      flex-grow:1;
    }
  }
`;

const Component= ({className,...props}) => {
  const mq = useMQ();

  const classes=['NavMenuStickyWrapper']
  className && classes.push(className)

  return (
    <Wrapper className={classes.join(' ')} mq={mq}>
      <div className="contentWrapper">
        <TopBar/>
      </div>
    </Wrapper>
  )
}

Component.displayName='NavMenuStickyWrapper'

export default Component
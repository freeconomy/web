import React from "react";
import styled from "styled-components";
import { cms } from "../lib/contentful";
import MD from "react-markdown";
import mq from "../hooks/mediaQuery";
import Box from "./Box";
import Carousel, { Modal, ModalGateway } from "react-images";

const Wrapper = styled.div`
  max-width: 1000px;
  margin: auto;
  .longDescription {
    font-size: 2rem;
  }

  .mediaGallery {
    display: flex;
    overflow-x: scroll;
    align-items: center;
    scroll-snap-type: x mandatory;
    margin-bottom: 5rem;

    li {
      flex-shrink: 0;
      width: 80%;
      margin-left: 3rem;
      scroll-snap-align: center;
    }
    img,
    button {
      display: block;
      max-width: 100%;
      border:none;
      padding:0;
    }
  }
`;

const boxPaddingMap = {
  xs: 3,
  sm: 5,
  md: 8
};

const Component = ({ className, id }) => {
  const [project, setProject] = React.useState({});
  const [modalIsOpen, setModalIsOpen] = React.useState(false);
  const [imgIndex, setImgIndex]= React.useState(0);
  const breakPoint = mq();

  function toggleModal() {
    setModalIsOpen(!modalIsOpen);
  }

  function showImage(index){
    return ()=>{
      setImgIndex(index);
      toggleModal()
    }
  }

  React.useEffect(() => {
    cms.getEntry(id).then(entry => {
      setProject({
        ...entry.fields,
        media: entry.fields.media.map(m => ({
          source: m.fields.file.url
        }))
      });
    });
  }, [id]);

  const classes = ["ProjectsContent"];
  className && classes.push(className);

  return (
    <Wrapper className={classes.join(" ")}>
      <Box p={'6 3'}>
        <h1>{project.titolo}</h1>
        <MD source={project.descrizioneLunga} className="longDescription" />
      </Box>

      {project.media && (
        <ul className="mediaGallery">
          {project.media.map((m, index) => (
            <li key={m.source}>
              <button onClick={showImage(index)}>
                <img alt={m.source} src={m.source} />
              </button>
            </li>
          ))}
        </ul>
      )}

      <ModalGateway>
        {(modalIsOpen && project.media) ? (
          <Modal onClose={toggleModal}>
            <Carousel currentIndex={imgIndex} views={project.media} />
          </Modal>
        ) : null}
      </ModalGateway>
    </Wrapper>
  );
};

Component.displayName = "ProjectsContent";

export default Component;

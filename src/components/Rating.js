import React from 'react';
import styled from 'styled-components';
import starPH from 'assets/images/starPH.svg';
import star from 'assets/images/starFull.svg';

const Wrapper = styled.div`
  position: relative;
  padding-bottom: 20%;
  width: 100%;
  
  &:before{
    position: absolute;
    content:"";
    width: 100%;
    height: 100%;
    background-image: url(${starPH});
    background-size: 20% 100%;
    opacity: 0.2;
  }

  &:after{
    position: absolute;
    content:"";
    width: ${({stars})=>stars * 100/ 5 }%;
    height: 100%;
    background-image: url(${star});
    background-size: auto 100%;
  }

`;

const Component= ({className, stars}) => {

  const classes=['Rating']
  className && classes.push(className)

  return (
    <Wrapper className={classes.join(' ')} stars={stars} />
  )
}

Component.displayName='Rating'

export default Component
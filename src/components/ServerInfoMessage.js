import useMessage from "lib/socketio";
import React from "react";
import InfoMessage from "components/popups/InfoMessage";
import AlertMessage from "components/popups/AlertMessage";
import ErrorMessage from "components/popups/ErrorMessage";
import ChatMessage from "components/popups/ChatMessage";
import styled from "styled-components";

const Wrapper = styled.div`
  position: fixed;
  right: 10px;
  top: 10px;
  z-index: 99;
  display: flex;
  flex-direction: column;
  max-height: 80vh;
  overflow: auto;
  padding: 5px;
  align-items: flex-end;
  max-width: 60vw;

  .BaseMessage {
    margin-bottom: 5px;
  }
`;

const msgTypeMap = {
  "Info": InfoMessage,
  "Alert": AlertMessage,
  "Error": ErrorMessage,
  "Chat": ChatMessage
}

const Component = () => {

  const {info, chat} = useMessage();
  const [messages, setMessages] = React.useState([])
  
  function removeMessage(msg){
    return ()=>{
      setMessages(old => {
        return old.filter(m=>m !== msg)
      })
    }
  }

  React.useEffect(()=>{
    if(info){
      setMessages(old => [info, ...old])
    }
  },[info])

  React.useEffect(()=>{
    if(chat){      
      setMessages(old => [chat, ...old])
    }
  },[chat])

  return (
    <Wrapper>
      {messages.map(msg => {
        const MsgComponent = msgTypeMap[msg.type];
        return <MsgComponent onClose={removeMessage(msg)} key={msg.timestamp} msgObj={msg}/>
      })}
    </Wrapper>
  );
};

Component.displayName = "ServerInfoMessage"
export default Component
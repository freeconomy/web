import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  ul {
    display: flex;
    overflow: auto;
    padding: 5px;
    background-color: #ddd;
    border-top: 1px solid #ccc;
  }
  button {
    background-color: transparent;
    border: none;
    margin: 0;
    padding: 0.5em 1em;
    display: block;
    border-radius: 3px;
    margin-right: 2px;
  }

  .alignRight {
    margin-left: auto;
  }

  .selected {
    button {
      box-shadow: 0 0 5px rgba(0, 0, 0, 0.25);
      background-color: white;
      font-weight: 600;
    }
  }
`;

const Component = ({ className, tabs }) => {
  const classes = ["TabMenu"];
  className && classes.push(className);

  return (
    <Wrapper className={classes.join(" ")}>
      <ul>
        {tabs.map(t => {
          const classes = [
            t.alignRight ? "alignRight" : null,
            t.selected ? "selected" : null
          ];
          return (
            <li key={t.label} className={classes.filter(c => !!c).join(" ")}>
              <button>{t.label}</button>
            </li>
          );
        })}
      </ul>
    </Wrapper>
  );
};

Component.displayName = "TabMenu";

export default Component;

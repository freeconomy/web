import React from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';

const Wrapper = styled.div`
  position: fixed;
  top:0;
  bottom:0;
  left:0;
  right:0px;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 999;

  &:before{
    content:"";
    position: absolute;
    top:0;
    bottom:0;
    left:0;
    right:0px;
    background-color:rgba(0,0,0,0.5);
  }
`;

const ContentWrapper = styled.div`  
  background-color:white;
  padding: 10px;
  border-radius: 5px;
  box-shadow: 0 0 5px rgba(0,0,0,0.5);
  position: relative;
  max-width: 99vw;
  max-height: 85vh;
  overflow: auto;
`

const BodyElement = document.body;

const Component= ({className, onClose, children, ...props}) => {

  const classes=['GenericModal']
  className && classes.push(className)

  return ReactDOM.createPortal(
    <Wrapper className={classes.join(' ')} onClick={onClose}>
      <ContentWrapper onClick={e=>{e.stopPropagation()}}>
        {children}
      </ContentWrapper>
    </Wrapper>,
    BodyElement
  )
}

Component.displayName='GenericModal'

export default Component
import React from 'react';
import styled from 'styled-components';
import GenericModal from 'components/modals/GenericModal';
import PostHeader from 'components/post/Header';
import PostPictures from 'components/post/Pictures';
import PostDetails from 'components/post/Details'
import PostMessages from 'components/post/Messages';

import moment from "moment";
import "moment/locale/es";
import "moment/locale/ca";
import useI18n from "hooks/i18n";

const Wrapper = styled(GenericModal)`
  .contentWrapper {
    width: 100vw;
    max-width: 100%;
  }
`;

const Component= ({className, post, ...props}) => {
  const loc = useI18n();
  moment.locale(loc);

  const classes=['ShowPost']
  className && classes.push(className)

  return (
    <Wrapper className={classes.join(' ')} {...props}>
      <div className="contentWrapper">
        <PostHeader title={post.title} updatedAt={moment(post.updated_at).format("DD MMM")} />
        <PostPictures pictures={post.pictures} gallery />
        <PostDetails owner={post.owner} description={post.description}/> 
        <PostMessages messages={post.messages} />
      </div>
    </Wrapper>
  )
}

Component.displayName='ShowPost'

export default Component
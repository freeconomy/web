import React from "react";
import { useGraphQl } from "lib/strapi";
import TopBar from "components/TopBar";
import Scrollable from "layouts/ScrollableContent";
import Freeshop from "components/Freeshop";
import useLS from "hooks/useStorage";
import MallorcaMap from "components/MallorcaMap";
import styled from "styled-components";
import { useSpring, animated } from "react-spring";
import useMeasure from "react-use-measure";
import { useDrag } from "react-use-gesture";
import useMQ, { breakpointsMap } from "hooks/mediaQuery";
import TabMenu from "components/TabMenu";

const Handle = styled.div`
  background-color: #f0f0f0;
  height: 10px;
  position: relative;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -o-user-select: none;
  user-select: none;
  box-shadow: 0 0px 2px rgba(0, 0, 0, 0.4);
  z-index: 3;

  &:before {
    position: absolute;
    content: "";
    left: calc(50% - 20px);
    width: 40px;
    top: 3px;
    height: 2px;
    border-top: 1px solid rgba(0, 0, 0, 0.4);
    border-bottom: 1px solid rgba(0, 0, 0, 0.4);
  }

  &:after {
    position: absolute;
    content: "";
    left: calc(50% - 30px);
    width: 60px;
    top: -25px;
    height: 60px;
    background-color: rgba(255, 255, 255, 0.5);
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
    border-radius: 100%;
  }
`;
const Wrapper = styled(Scrollable)`
  .contentWrapper {
    display: flex;
    flex-direction: column;
    height: 100%;
    @media screen and (${breakpointsMap.md}) {
      flex-direction: row;
    }
  }
`;

const MapWrapper = styled.section`
  flex-grow: 1;
  background-color: #fff;
  .leaflet-container {
  }
`;
const PostsWrapper = styled(animated.div)`
  flex-shrink: 0;
  height: 25%;
  overflow: auto;
  background-color: #fff;
  display: flex;
  flex-direction: column;
  .postList {
    flex-grow: 1;
  }

  @media screen and (${breakpointsMap.md}) {
    width: 30%;
    max-width: 500px;
    height: 100%;
    border: none;
    border-left: 1px solid #bbb;
  }
`;

const Component = ({ className }) => {
  const data = useGraphQl("freeshopPosts") || { posts: [] }; 

  const classes = ["Freeshop"];
  className && classes.push(className);
  const [auth] = useLS("auth");
  const mq = useMQ();

  const [contentWrapperRref, contentWrapperBounds] = useMeasure();

  const minHeight = 100;
  const maxHeight = contentWrapperBounds.height - minHeight;

  const [{ height: listHeight }, set] = useSpring(() => ({
    height: minHeight
  }));

  let [currentHeight, setCurrentHeight] = React.useState(minHeight);
  set({ height: currentHeight });

  // Set the drag hook and define component movement based on gesture data
  const bind = useDrag(drag => {
    const {
      down,
      movement: [mx, my]
    } = drag;
    const distance = Math.abs(my);

    const target =
      distance < 100
        ? currentHeight
        : currentHeight === minHeight
        ? maxHeight
        : minHeight;
    let newH = down ? currentHeight - my : target;

    // maybe it's a click?
    if (!down && distance < 5) {
      newH = currentHeight === minHeight ? maxHeight : minHeight;
    }
    if (!down) {
      // currentHeight = newH
      setCurrentHeight(newH)
    }
    set({ height: newH });
  });

  const markers = data.posts.map(fs => ({
    ...fs,
    loc: fs.location
  }));

  function handleMarkerClick(d) {
    console.log(d);
  }

  const tabs = [
    { label: "Offers", selected: true },
    { label: "Requests" },
    {
      label: auth ? auth.user.username : "Login",
      alignRight: true
    }
  ];

  return (
    <Wrapper top={<TopBar />}>
      <div className="contentWrapper" ref={contentWrapperRref}>
        <MapWrapper>
          <MallorcaMap
            markers={markers}
            onMarkerClick={handleMarkerClick}
            maxZoomLevel={15}
          />
        </MapWrapper>
        {mq !== "md" && <Handle {...bind()} />}

        <PostsWrapper style={{ height: mq !== "md" ? listHeight : "auto" }}>
          {/* {auth && (<h2>Hello {auth.user.username}</h2>)} */}
          <Freeshop className="postList" posts={data && data.posts} />
          {mq === "md" && <TabMenu tabs={tabs} />}
        </PostsWrapper>
        {mq !== "md" && <TabMenu tabs={tabs} />}
      </div>
    </Wrapper>
  );
};

Component.displayName = "Freeshop";

Component.route = `/freeshop/:postId?`;

Component.path = postId => {
  return postId ? `/freeshop/${postId}?` : "/freeshop";
};

export default Component;

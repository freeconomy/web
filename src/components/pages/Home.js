import React from "react";
import styled from "styled-components";

import Header from "components/Header";
import NavMenuStickyWrapper from "components/NavMenuStickyWrapper";
import PageMainContent from "components/PageMainContent";
import Activities from "components/Activities";
import ActiviesQuickLinks from "components/ActivitiesQuickLinks";
import MallorcaMap from "components/MallorcaMap";
import { breakpointsMap } from "hooks/mediaQuery";

import Mission from "components/Mission";
import { useI18Nfetch } from "hooks/i18n";
import { useFetch } from "lib/strapi";

import { ReactComponent as Forum } from "assets/images/forum.svg";
import { ReactComponent as Telegram } from "assets/images/telegram.svg";

import Popup from 'components/MapPopup'


const Wrapper = styled.div`
  > .PageMainContent {
    .PageMainContentWrapper {
      position: relative;
      margin: -10px auto;
    }
  }

  .MallorcaMap{
    padding: 50px 0px;
    background-color: white; 
    .leaflet-container {
      max-width: 1000px;
      margin: auto;
      height: 40vh;
    }

    @media screen and (${breakpointsMap.sm}) {
    .leaflet-container {
      height: 30vh;
    }
  }
  }
`;

const Component = ({ className, ...props }) => {
  const activitiesList = useI18Nfetch("activities");
  const mission = useI18Nfetch("mission");
  const liveActivities = useFetch("live-activities") || [];

  const markers = liveActivities.map(la => ({
    loc: la.location,
    title: la.name,
    type: la.activity && la.activity.Icon,
    popup: (
      <Popup>
        <p>{la.name}</p>
        <a href={la.url} target="_blank" rel="noopener noreferrer">
          <Forum />
          Forum
        </a>
        {la.Telegram_group && (
          <a href={la.Telegram_group} target="_blank" rel="noopener noreferrer">
            <Telegram />
            Telegram group
          </a>
        )}
      </Popup>
    )
  }));

  function handleMarkerClick(m) {
    // I don't need this right now, the popup element handles it for me
    console.log(m);
  }

  const classes = ["Home"];
  className && classes.push(className);

  const missionData = mission && mission.i18n;

  return (
    <Wrapper>
      <Header />
      <NavMenuStickyWrapper />
      <ActiviesQuickLinks
        data={activitiesList}
        title={missionData && missionData.title}
        subtitle={missionData && missionData.subtitle}
      />
      <PageMainContent>
        <Mission description={missionData && missionData.description} />
      </PageMainContent>
      <MallorcaMap markers={markers} onMarkerClick={handleMarkerClick} />
      <Activities data={activitiesList} />
    </Wrapper>
  );
};

Component.displayName = "Home";

export default Component;

import React from "react";
import styled from 'styled-components';
import BaseMessage from './BaseMessage';

const Component = styled(BaseMessage).attrs(props => ({
  timeout:6000,
  className:'AlertMessage',
  ...props
}))
`
  background-color:yellow;
  color:#555;
`;

Component.displayName='AlertMessage'

export default props => {
  const { msg, ...rest } = props;
  return <Component {...rest}>{msg}</Component>;
};
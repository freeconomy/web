import React from "react";
import styled from "styled-components";

const Wrapper = styled.button`
  background-color: #999;
  border-radius: 6px;
  border: none;
  border:2px solid rgba(255,255,255,0.8);
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.4);
  color: white;
  cursor: pointer;
  font-size: 14px;
  padding: 10px;
`;

const Component = (props) => {
  const { className, children, timeout, onClose}=props;

  const classes = ["BaseMessage"];
  className && classes.push(className);


  React.useEffect(()=>{
    
    if(timeout){
      const hnd = setTimeout(()=>{
        onClose();
      }, timeout)

      return ()=>{
        clearTimeout(hnd)
      }
    }
  },[timeout])

  return <Wrapper onClick={onClose} className={classes.join(" ")}>{children}</Wrapper>;
};

Component.displayName = "BaseMessage";

export default Component;

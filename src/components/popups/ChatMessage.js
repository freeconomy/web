import React from "react";
import styled from "styled-components";
import BaseMessage from "./BaseMessage";

const ChatMessage = styled(BaseMessage).attrs(props => ({
  timeout: 9000,
  className: "ChatMessage",
  ...props
}))`
  width: 100%;
  background-color: #65a65c;
  display: flex;
  align-items: center;
  border-radius: 50px;
  padding: 3px;
  .avatar {
    width: 30px;
    border-radius: 30px;
    flex-shrink: 0;
    margin-right: 0.3em;
  }

  .messageBody {
    flex: 1;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    p {
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
      margin: 0;
    }
  }
`;

const Component = props => {
  const { msgObj, ...rest } = props;
  return (
    <ChatMessage {...rest}>
      <img
        className="avatar"
        alt="User"
        src={`https://www.peppercarrot.com/extras/html/2019_bird-generator/avatar.php?seed=${msgObj.sender}&size=50`}
      />
      <div className="messageBody">
        <p>{msgObj.msg}</p>
      </div>
    </ChatMessage>
  );
};

ChatMessage.displayName = "ChatMessage";

export default Component;

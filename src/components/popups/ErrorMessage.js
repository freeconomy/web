import React from "react";
import styled from 'styled-components';
import BaseMessage from './BaseMessage';

const Component = styled(BaseMessage).attrs(props => ({
  timeout:9000,
  className:'ErrorMessage',
  ...props
}))`
  background-color:red;
  color:white;
`;

Component.displayName='ErrorMessage'

export default props => {
  const { msg, ...rest } = props;
  return <Component {...rest}>{msg}</Component>;
};
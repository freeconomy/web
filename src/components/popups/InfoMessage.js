import React from "react";
import styled from "styled-components";
import BaseMessage from "./BaseMessage";

const Component = styled(BaseMessage).attrs(props => ({
  timeout: 6000,
  className: "InfoMessage",
  ...props
}))`
  background-color: #4d89e0;
`;

Component.displayName = "InfoMessage";

export default props => {
  const { msgObj, ...rest } = props;
  return <Component {...rest}>{msgObj.msg}</Component>;
};

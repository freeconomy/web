import React from 'react';
import styled from 'styled-components';
import Rating from "components/Rating";
import Ellipsis from "components/Ellipsis";

const Wrapper = styled.div`
    display: flex;
    .avatar {
      margin-right: 10px;
      width: 50px;
      display: flex;
      flex-direction: column;
      align-items: center;
      flex-shrink: 0;
      img {
        max-width: 100%;
        max-height: 40px;
      }
    }
    .description {
      display: flex;
      flex-direction: column;
    }
    .owner {
      font-size: 1.5em;
      font-weight: 400;
      margin: 0.5em 0;
    }
    .descriptionText {
      font-size: 2em;
      flex-grow: 1;
    }
`;

const Component= ({className, owner, description, maxLength, ...props}) => {

  const classes=['Details']
  className && classes.push(className)

  return (
    <Wrapper className={classes.join(' ')}>
        <div className="avatar">
          <img
            alt={owner.username}
            src={`https://www.peppercarrot.com/extras/html/2019_bird-generator/avatar.php?seed=${owner.username}&size=80`}
          />
          <Rating stars={3} />
        </div>

        <div className="description">
          <span className="owner">{owner.name}</span>
          <Ellipsis
            className="descriptionText"
            text={description}
            maxLength={maxLength}
          />
        </div>  
    </Wrapper>
  )
}

Component.displayName='Details'

export default Component
import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  .date {
    font-size: 1.4em;
    font-weight: 400;
    flex-shrink: 0;
  }
  h3 {
    font-weight: 400;
  }
`;

const Component= ({className, title, updatedAt, ...props}) => {
  
  const classes=['postHeader']
  className && classes.push(className)

  return (
    <Wrapper className={classes.join(' ')}>
      <h3>{title}</h3>
      <span className="date">
        {updatedAt}
      </span>
    </Wrapper>
  )
}

Component.displayName='postHeader'

export default Component
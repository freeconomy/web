import React from 'react';
import styled from 'styled-components';
import moment from 'moment';

const Wrapper = styled.div`
  .message {
    width: 100%;
    background-color: #cfe8cb;
    display: flex;
    align-items: center;
    border-radius: 5px;
    padding: 3px;
    box-shadow: 0 1px 4px rgba(0,0,0,0.3);
    margin-bottom: 5px;
    max-width: 90%;
    font-size: 1.4rem;

    .avatar {
      width: 30px;
      border-radius: 30px;
      flex-shrink: 0;
      margin-right: 0.3em;
    }

    .messageBody {
      flex: 1;
      p{
        margin-bottom: 0.5em;
      }

      .messageText {
        font-weight: 400;
        font-size: 1.4em;
      }
    }

    .sender {
      display: flex;
      justify-content: space-between;
    }

    .timestamp {
      font-weight: 400;
      font-size:0.8em;
    }

  }
`;

const Component= ({className,messages}) => {

  const classes=['Messages']
  className && classes.push(className)

  return (
    <Wrapper className={classes.join(' ')}>
      <ul>
        {messages.map(m=>{
          const timestamp = moment(m.timestamp);
          return (
          <li className='message' key={m.id}>
            <img
              className="avatar"
              alt="User"
              src={`https://www.peppercarrot.com/extras/html/2019_bird-generator/avatar.php?seed=${m.from.username}&size=50`}
              />
            <div className="messageBody">
              <p className="sender">
                <span className="username">
                  {m.from.username}
                </span>
                <span className="timestamp">
                  {timestamp.isSame('day') ? timestamp.format("hh:mm") : timestamp.format("DD/MM hh:mm")}
                </span>
              </p>
              <p className='messageText'>{m.message}</p>
            </div>            
          </li>
        )})}
      </ul>
    </Wrapper>
  )
}

Component.displayName='Messages'

export default Component
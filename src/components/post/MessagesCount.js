import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  align-items: center;
  background-color: yellow;
  background-color:${({count}) => count ? `#ffba02`:`#f0f0f0`};
  border-radius: 3px;
  display: flex;
  font-weight:600;
  height: 1.4em;
  justify-content: center;
  line-height: 0.8;
  position: relative;
  width: 2em;
  margin-bottom: 8px;
  span {
    margin-top: 0.1em;
  }
  &:before{
    position: absolute;
    content: "";
    bottom: -14px;
    left: 0px;
    border: 8px solid transparent;
    border-left: 0px;
    border-top-color: ${({count}) => count ? `#ffba02`:`#f0f0f0`};
  }
`;

const Component= ({className, count}) => {

  const classes=['MessagesCount']
  className && classes.push(className)

  return (
    <Wrapper className={classes.join(' ')} count={count}>
      <span>{count}</span>
    </Wrapper>
  )
}

Component.displayName='MessagesCount'

export default Component
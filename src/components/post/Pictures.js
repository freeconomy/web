import React from "react";
import styled from "styled-components";
import { basePath } from "lib/config";
import Carousel, { Modal, ModalGateway } from "react-images";

const PicWrapper = styled.div`
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
  position: absolute;
  border-radius: 3px;
  border: 3px solid white;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);

  img {
    position: absolute;
    width: 100%;
    margin: auto;
  }
`;

const Wrapper = styled.div`
  padding-bottom: 50%;
  position: relative;
  margin: 10px 20px;

  .stackedImages {
    &:before {
      content: "";
      position: absolute;
      height: 100%;
      width: 100%;
      border-radius: 3px;
      border: 3px solid white;
      box-shadow: 0 0px 3px rgba(0, 0, 0, 0.3);
      transform: rotateZ(3deg);
      transform-origin: 50%;
      background-color: #e0e0e0;
      left: 0;
    }
  }

  .gallery {
    display: flex;
    flex-wrap: nowrap;
    height: 100%;
    width: 100%;
    position: absolute;
    overflow-x: scroll;
    scroll-snap-type: x mandatory;
    img {
      max-height: 100%;
      margin-right: 5px;
      scroll-snap-align: center;
    }
  }
`;

const Component = ({ className, pictures = [], gallery = false }) => {
  const classes = ["Pictures"];
  className && classes.push(className);

  const [isGalleryOpen, setIsGalleryOpen] = React.useState(false);
  const [imgIndex, setImgIndex] = React.useState(0);

  const stackedImages = pictures.length > 1 ? "stackedImages" : "";

  const pic = pictures[0];

  const picsMap = pictures.map(i => ({
      source: basePath + i.formats.medium.url
  }))

  function toggleGallery(){
    setIsGalleryOpen(!isGalleryOpen)
  }
  
  function showImage(index) {
    return () => {
      setImgIndex(index);
      toggleGallery();
    };
  }

  return (
    <Wrapper className={classes.join(" ")}>
      {pic &&
        (gallery ? (
          <>
          <ModalGateway>
          {isGalleryOpen && picsMap ? (
            <Modal onClose={toggleGallery}>
              <Carousel currentIndex={imgIndex} views={picsMap} />
            </Modal>
          ) : null}
        </ModalGateway>


          <div className="gallery">
            {pictures.map((picture, index) => (
                <img
                  onClick={showImage(index)}
                  key={picture.formats.medium.url}
                  alt={picture.title}
                  src={basePath + picture.formats.medium.url}
                  draggable={false}
                />
            ))}
          </div>
          </>
        ) : (
          <div className={`${stackedImages}`}>
            <PicWrapper key={pic.formats.medium.url}>
              <img
                alt={pic.title}
                src={basePath + pic.formats.medium.url}
                draggable={false}
              />
            </PicWrapper>
          </div>
        ))}
    </Wrapper>
  );
};

Component.displayName = "Pictures";

export default Component;

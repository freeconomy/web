import React from 'react';

function safeParseLS(key){
  let parsedValue=undefined;

  const item = window.localStorage.getItem(key);

  try {
    parsedValue = JSON.parse(item)
  } catch (error) {
    console.warn('error while parsing local storage')
  }
  return parsedValue
}

export default (key, initialValue) => {

  // Get from local storage by key
  const item = safeParseLS(key);
  
  // State to store our value
  // Pass initial state function to useState so logic is only executed once
  const [storedValue, setStoredValue] = React.useState(item || initialValue);


  // Return a wrapped version of useState's setter function that ...
  // ... persists the new value to localStorage.
  const setValue = value => {
    try {
      // Allow value to be a function so we have same API as useState
      const valueToStore =
        value instanceof Function ? value(storedValue) : value;
      // Save state
      setStoredValue(valueToStore);
      // Save to local storage
      window.localStorage.setItem(key, JSON.stringify(valueToStore));
    } catch (error) {
      // A more advanced implementation would handle the error case
      console.log(error);
    }
  };

  React.useEffect(()=>{
    setStoredValue(item || initialValue)
  },[initialValue, item])

  return [storedValue, setValue];
}
import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  .topSection {
    flex-shrink:0;
    position: relative;
    z-index: 2;
  }
  
  .mainSection {
    flex-grow: 1;
    overflow: auto;
  }
`;

const Component= ({className,top, children}) => {

  const classes=['ScrollableContent']
  className && classes.push(className)

  return (
    <Wrapper className={classes.join(' ')}>
      <section className='topSection'>{top}</section>
      <section className='mainSection'>{children}</section>
    </Wrapper>
  )
}

Component.displayName='ScrollableContent'

export default Component
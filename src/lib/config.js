const urlMap={
  production:'https://cms.freeconomy.network',
  development: `http://${window.location.hostname}:1337`
}

const wsMap={
  production:'https://ws.freeconomy.network',
  development: `http://${window.location.hostname}:1338`
}

export const basePath=urlMap[process.env.NODE_ENV]
export const wsPath=wsMap[process.env.NODE_ENV]
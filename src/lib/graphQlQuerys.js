export default {
  freeshopPosts: `
    {
      posts:freeshops {
        id
        title
        description
        active
        type
        owner{
          username
          nickname
          picture{
            url
          }
          id
        }
        location{
          lat
          lng
        }
        pictures{
          formats
        }
        category:freeshop_category{
          i18n:category{
            name
            language
          }
        }
        messages:freeshop_messages{
          from{
            username
            picture {
              url
            }
          }
          message
          timestamp: created_at
        }
      }
    }  
  `
};

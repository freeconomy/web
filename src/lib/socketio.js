// import io from "socket.io-client";
// import { wsPath } from "lib/config";

// export const msgType = {
//   chat: "Chat",
//   info: "Info",
//   alert: "Alert",
//   error: "Error"
// };

// const socket = io(wsPath);
// console.log("Connecting to WS...");

// socket.on("connect", function() {
//   console.log("✅ Connected!");
// });

// socket.on("disconnect", function() {
//   console.log("❌ Disconnected!");
// });

// export const onChat = cb => {
//   socket.on("Chat", cb)
//   return () => {
//       socket.off("Chat", cb);
//     };
// };
// export const onInfo = cb => {
//   socket.on("Info", cb)
//   return () => {
//       socket.off("Info", cb);
//     };
// };
// export const onAlert = cb => {
//   socket.on("Alert", cb)
//   return () => {
//       socket.off("Alert", cb);
//     };
// };
// export const onError = cb => {
//   socket.on("Error", cb)
//   return () => {
//       socket.off("Error", cb);
//     };
// };

import io from "socket.io-client";
import { wsPath } from "lib/config";
import React from 'react';

export const msgType = {
  chat: "Chat",
  info: "Info",
  alert: "Alert",
  error: "Error"
};

const socket = io(wsPath);
console.log("Connecting to WS...");

socket.on("connect", function() {
  console.log("✅ Connected!");
});

socket.on("disconnect", function() {
  console.log("❌ Disconnected!");
});

export default ()=>{

  const messages={
    Chat:React.useState(),
    Info:React.useState(),
    Alert:React.useState(),
    Error:React.useState(),
  }

  function setMessage(type){
    return (msg)=>{
      const setMsgs = messages[type][1]
      setMsgs({
        ...msg,
        type
      })
    }
  }

  React.useEffect(()=>{
    const chatFn=setMessage('Chat');
    const infoFn=setMessage('Info')
    const alertFn=setMessage('Alert')
    const errorFn=setMessage('Error')

    socket.on("Chat", chatFn)
    socket.on("Info", infoFn)
    socket.on("Alert", alertFn)
    socket.on("Error", errorFn)

    return ()=>{
      socket.off("Chat", chatFn)
      socket.off("Info", infoFn)
      socket.off("Alert", alertFn)
      socket.off("Error", errorFn)
    }
  },[])

  return {
    chat:messages.Chat[0],
    info:messages.Info[0],
    alert:messages.Alert[0],
    error:messages.Error[0]
  }
}




